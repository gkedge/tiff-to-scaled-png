#ifndef nsclock_h
#define nsclock_h

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __WIN32__
typedef long long point_in_time_t;
#else  // NOT __WIN32__
typedef double point_in_time_t;
#endif

#ifdef __WIN32__
void init_win32_frequency();
#endif // __WIN32__

point_in_time_t get_time_now();
double get_elapsed_time_sec(point_in_time_t start);
double get_elapsed_time_ms(point_in_time_t start);
double get_elapsed_time_ns(point_in_time_t start);
int get_time_stamp(char *, int);

#ifdef __cplusplus
}
#endif

#endif // nsclock_h
