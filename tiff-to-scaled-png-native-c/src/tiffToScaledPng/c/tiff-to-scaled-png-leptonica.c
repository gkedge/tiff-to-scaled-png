#include <string.h>

/*
 * All leptonica specific code is concentrated within this single file. Should
 * interest be extended to consider other native image libraries, implement
 * the signatures declared in tiff-to-scaled-png.h in a separate library-specific
 * source file.  Add another shared library target to the build system
 * that is library-specific.
 */
#include <zlib.h>
#include <allheaders.h>

#include "nsclock.h"
#include "tiff-to-scaled-png.h"

static PIX *decompress_tiff_leptonica(JNIEnv *env, image_state_t *image_state);
static PIX *scale_image_leptonica(JNIEnv *env, image_state_t *image_state,
                                  PIX *source, int dest_width, int dest_height);
int isGPUDeviceAvailable();
PIX *scale_image_leptonica_cuda(JNIEnv *env, image_state_t *image_state,
                                PIX *source, int dest_width, int dest_height);
static void compress_png_leptonica(JNIEnv *env, image_state_t *image_state, PIX *source);
void logPIXImage(JNIEnv *env, image_state_t *image_state, PIX *pix);

void convert_tiff_to_scaled_png_leptonica(JNIEnv *env, image_state_t *image_state)
{
    point_in_time_t start_time = 0;
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
        if (debug_log_level >= LOG_LEVEL_DEBUG)
        {
            log_entry("convert_tiff_to_scaled_png_leptonica: starting imaging chain",
                      image_state);
        }
    }

    PIX *source = decompress_tiff_leptonica(env, image_state);
    if (get_native_test() != 1 && (*env)->ExceptionOccurred(env) != NULL)
    {
        return;
    }

    /* Compute height without skewing the aspect ratio
       between source and destination image */
    int png_width = image_state->png_width;
    int png_height = (png_width * (source->h)) / (source->w);

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->png_height = png_height;
    }

    PIX *scaleDest;
    if (source->w != png_width)
    {
/* 
        int image_chain = image_state->image_chain;
        int isRunGPU = image_chain == LEPTONICA_CUDA_CONVERSION_PATH ||
                       image_chain == LEPTONICA_HYBRID_CONVERSION_PATH;

        isRunGPU = isRunGPU && get_has_gpu();
        isRunGPU = isRunGPU && (image_chain == LEPTONICA_CUDA_CONVERSION_PATH 
            || (image_chain == LEPTONICA_HYBRID_CONVERSION_PATH && isGPUDeviceAvailable()));

        scaleDest = isRunGPU ?
            scale_image_leptonica_cuda(env, image_state, source, png_width, png_height) :
            scale_image_leptonica(env, image_state, source, png_width, png_height);
 */
        scaleDest = scale_image_leptonica(env, image_state, source, png_width, png_height);

        pixDestroy(&source);
        // Testing the scaling call, not the pix destruction...
        if (get_native_test() != 1 && (*env)->ExceptionOccurred(env) != NULL)
        {
            return;
        }
    }
    else
    {
        scaleDest = source;
    }

    compress_png_leptonica(env, image_state, scaleDest);

    pixDestroy(&scaleDest);

    if (get_native_test() != 1 && (*env)->ExceptionOccurred(env) != NULL)
    {
        return;
    }

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_convert_time =
            get_elapsed_time_ms(start_time);
        if (debug_log_level >= LOG_LEVEL_DEBUG)
        {
            log_entry("convert_tiff_to_scaled_png: returning from leptonica imaging chain",
                      image_state);
        }
    }
}

static PIX *decompress_tiff_leptonica(JNIEnv *env, image_state_t *image_state)
{
    point_in_time_t start_time = 0;
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
    }

    /* The obtained TIFF image is expected to be a single band of 8bit gray scale data. */
    unsigned char *tiff_image = image_state->tiff_image;
    int tiff_compressed_size = image_state->tiff_compressed_size;
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        unsigned char *internal_image_chars = image_state->tiff_image;
        char byte_order = internal_image_chars[0];
        int motorola_big_endian = byte_order == 'M';
        // int intel_little_endian = byte_order == 'I';
        unsigned int directoryAddress = ((internal_image_chars[motorola_big_endian ? 4 : 7]) << 24) |
                                        ((internal_image_chars[motorola_big_endian ? 5 : 6]) << 16) |
                                        ((internal_image_chars[motorola_big_endian ? 6 : 5]) << 8) |
                                        internal_image_chars[motorola_big_endian ? 7 : 4];
        // A TIFF image signature (or magic number) should either be:
        // II/2A/directory or MM/00/directory
        char signature_buf[strlen("TIFF: Size: -2147483647; Signature: [II\0042\0x00000000\0000000000]") + 100];
        sprintf(signature_buf,
                "TIFF: Size: %d; Signature: [%c%c\\%02d%02d\\0x%x\\%u]",
                image_state->tiff_compressed_size,
                internal_image_chars[0], internal_image_chars[1],
                internal_image_chars[motorola_big_endian ? 2 : 3],
                internal_image_chars[motorola_big_endian ? 3 : 2],
                directoryAddress, directoryAddress);
        char msg[] = "decompress_tiff_lept:: starting TIFF decompression:";
        char log_msg[strlen(msg) + strlen(signature_buf) + 10];
        sprintf(log_msg, "%s %s", msg, signature_buf);
        log_entry(log_msg, image_state);
    }

    PIX *source = pixReadMemTiff(tiff_image, tiff_compressed_size, 0);

    /* Liberate the TIFF image data; we are done with it. */
    if (get_native_test() == 0) {
        free(image_state->tiff_image);
    }

    if (NULL == source)
    {
        const char *msg = "decompress_tiff_lept: in-memory image decode failed.";
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return NULL;
    }
    else if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_decompress_time = get_elapsed_time_ms(start_time);
        image_state->tiff_width = source->w;
        image_state->tiff_height = source->h;
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("decompress_tiff_lept: decompressed TIFF",
                      image_state);
        }
    }

    return source;
}

static PIX *scale_image_leptonica(JNIEnv *env, image_state_t *image_state,
                                  PIX *source, int dest_width, int dest_height)
{
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("scale_image_lept: starting image scaling",
                  image_state);
    }

    point_in_time_t start_time = 0;
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
    }

    PIX *scaled;
    if (source->w > dest_width) {
        float sourceWidth = (float)source->w;
        float scalefactor = dest_width/sourceWidth;
        if (scalefactor >= 0.0625 && scalefactor <= 0.5) {
            scaled = pixScaleToGrayFast(source, scalefactor);
        }
        else {
            scaled = pixScaleToGray(source, scalefactor);
        }
    }
    else {
        scaled = pixScaleToSize(source, dest_width, dest_height);
    }

    if (NULL == scaled)
    {
        const char *msg = "scale_image_lept: failed to create scale image.";
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return NULL;
    }
    else if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_scale_time = get_elapsed_time_ms(start_time);
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            // Not working yet... logPIXImage(env, image_state, scaled);
            log_entry("scale_image_lept: finished image scaling",
                      image_state);
        }
    }

    return scaled;
}

static void compress_png_leptonica(JNIEnv *env, image_state_t *image_state, PIX *source)
{
    point_in_time_t start_time = 0;
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("compress_png_lept: starting PNG encoding",
                      image_state);
        }
    }

    pixSetZlibCompression(source, get_png_compression_level());

    size_t png_compressed_size;
    int status = pixWriteMemPng(&(image_state->png_image),
                                &png_compressed_size,
                                source, 0.0);

    image_state->png_compressed_size = (int)png_compressed_size;

    if (status != 0)
    {
        const char *msg = "compress_png_lept: failed to encode PNG image.";
        if (image_state->png_image != NULL) {
            free(image_state->png_image);
            image_state->png_image = NULL;
            image_state->png_alloced_size = 0;
            image_state->png_alloced_size = 0;
        }
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return;
    }
    else if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_encode_png_time = get_elapsed_time_ms(start_time);
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("compress_png_lept: finished PNG encoding",
                      image_state);
        }
    }
}

void logPIXImage(JNIEnv *env, image_state_t *image_state, PIX *pix) {

    if (get_debug_log_level() < LOG_LEVEL_TRACE) {
        return;
    }

    if (pix->w * pix->h > 5000) {
        log_entry("logPIXImage: image too large to log", image_state);
        return;
    }
    PIX pix8Tmp;
    PIX *pix8 = pixCopy(&pix8Tmp, pix);
    if ((pix8->d) == 1) {
        pix8 = pixConvert1To8(NULL, pix8, 255, 0);
    }
    long v = 0x04030201;
    int endian = *((unsigned char *)(&v)) == 0x04 ? 1 : 0;

    if (endian == 0)
        pixEndianByteSwap(pix8);

    unsigned int bpl = 4 * pix8->wpl;
    unsigned int width = pix8->w;
    unsigned int height = pix8->h;
    unsigned char *imageData = (unsigned char *)pix8->data;
    unsigned int *imageDataPtr = pix8->data;

    char log_pix0_msg[strlen("PIX:: 00000(w) x 00000(h); 000(bpl), 0x00000000(image address)") + 10];
    sprintf(log_pix0_msg, "PIX:: %d(w) x %d(h); %d(bpl), 0x%08x(image address)",
            width, height, bpl, imageData);
    log_entry(log_pix0_msg, image_state);

    int ii = 0, jj = 0;
    char log_image_line_data_msg[strlen("0x00000000\t") + (height * bpl * 3) + 10];
    for (ii = 0; ii < height; ii++, imageDataPtr++)
    {
        log_image_line_data_msg[0] = '\0';
        char line_address[strlen("0x00000000\t") + 1];
        sprintf(line_address, "0x%08x\t", imageDataPtr);
        strcat(log_image_line_data_msg, line_address);
        for (jj = 0; jj < bpl; ++jj)
        {
            char byte_value[strlen("ff ") + 1];
            sprintf(byte_value, "%2x ", *(imageData + (ii * bpl) + jj));
            strcat(log_image_line_data_msg, byte_value);
        }
        log_entry(log_image_line_data_msg, image_state);
    }

    pixDestroy(&pix8);
}


