#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#ifndef __WIN32__
#include <pthread.h>
#endif // __WIN32__
#include <string.h>
#include <errno.h>
#include <zlib.h>
#include "state.h"
#include "nsclock.h"

static jfieldID g_u_p_image_State_Debug_Level_FID;
static jfieldID g_u_p_image_Png_Compression_Level_FID;
static jfieldID g_u_p_image_State_Log_Path_FID;

static jfieldID g_u_p_image_State_RequestedResource_FID;
static jfieldID g_u_p_image_State_TiffImage_FID;
static jfieldID g_u_p_image_State_TiffWidth_FID;
static jfieldID g_u_p_image_State_TiffHeight_FID;
static jfieldID g_u_p_image_State_TiffCompressedSize_FID;
static jfieldID g_u_p_image_State_ImageChain_FID;
static jfieldID g_u_p_image_State_ImageConvertTime_FID;
static jfieldID g_u_p_image_State_ImageDecompressTime_FID;
static jfieldID g_u_p_image_State_ImageScaleTime_FID;
static jfieldID g_u_p_image_State_ImageEncodePngTime_FID;
static jfieldID g_u_p_image_State_GpuWaitTime_FID;
static jfieldID g_u_p_image_State_GpuDevice_FID;
static jfieldID g_u_p_image_State_TotalNativeTime_FID;
static jfieldID g_u_p_image_State_PngImage_FID;
static jfieldID g_u_p_image_State_PngWidth_FID;
static jfieldID g_u_p_image_State_PngHeight_FID;

static int Debug_Level = LOG_LEVEL_TELEMETRY;
static int Png_Compression_Level = Z_BEST_SPEED;
static const char *Log_Path = "test.log";
static int Native_Test = 0;

static int has_gpu = -1;

static FILE *tiff_to_scaled_png_log;
#ifndef __WIN32__
static pthread_mutex_t log_mutex;
#endif // __WIN32__

/* This is only used if a request does not provide a 'requested
   resource' string for logging. Since it would be a global
   variable, it will have to have be synchronized to increment.
   See jni_GetRequestedResource_from_State()
*/
static int Missing_Image_Number;
#ifndef __WIN32__
static pthread_mutex_t image_number_mutex;
#endif // __WIN32__

void initStateFieldIDs(JNIEnv* env, jclass g_u_p_image_State_class)
{
    g_u_p_image_State_Debug_Level_FID =
        (*env)->GetStaticFieldID(env, g_u_p_image_State_class, "CONVERTER_LEVEL_LOG", "I");
    g_u_p_image_Png_Compression_Level_FID =
        (*env)->GetStaticFieldID(env, g_u_p_image_State_class, "PNG_COMPRESSION_LEVEL", "I");
    g_u_p_image_State_Log_Path_FID =
        (*env)->GetStaticFieldID(env, g_u_p_image_State_class, "NATIVE_LOG_PATH", "Ljava/lang/String;");

    g_u_p_image_State_RequestedResource_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "requestedResource", "Ljava/lang/String;");
    g_u_p_image_State_TiffImage_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "tiffImage", "[B");
    g_u_p_image_State_TiffWidth_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "tiffWidth", "I");
    g_u_p_image_State_TiffHeight_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "tiffHeight", "I");
    g_u_p_image_State_TiffCompressedSize_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "tiffCompressedSize", "I");
    g_u_p_image_State_ImageChain_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "imageChain", "I");
    g_u_p_image_State_ImageConvertTime_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "imageConvertTime", "D");
    g_u_p_image_State_ImageDecompressTime_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "imageDecompressTime", "D");
    g_u_p_image_State_ImageScaleTime_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "imageScaleTime", "D");
    g_u_p_image_State_ImageEncodePngTime_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "imageEncodePngTime", "D");
    g_u_p_image_State_TotalNativeTime_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "totalNativeTime", "D");
    g_u_p_image_State_GpuDevice_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "gpuDevice", "I");
    g_u_p_image_State_GpuWaitTime_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "gpuWaitTime", "D");
    g_u_p_image_State_PngImage_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "pngImage", "[B");
    g_u_p_image_State_PngWidth_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "pngWidth", "I");
    g_u_p_image_State_PngHeight_FID =
        (*env)->GetFieldID(env, g_u_p_image_State_class, "pngHeight", "I");
}

int get_debug_log_level()
{
    return Debug_Level;
}

void set_debug_log_level(int debug)
{
    Debug_Level = debug;
}

int get_native_test()
{
    return Native_Test;
}

void set_native_test(int native)
{
    Native_Test = native;
}

int get_png_compression_level() {
    return Png_Compression_Level;
}

int get_has_gpu() {
    return has_gpu;
}

void set_has_gpu(int gpu) {
    has_gpu = gpu;
}

const char *get_log_path()
{
    return Log_Path;
}

char *get_time_log_number_str(image_state_t *image_state)
{
    char datetime[25];
    get_time_stamp(datetime, sizeof(datetime));

    int hasState = image_state != NULL;
    int hasRequestedResource = hasState &&
                               image_state->requested_resource != NULL;
    int size = strlen(datetime) + 10;
    size = size + (hasRequestedResource ?
                   strlen(image_state->requested_resource) : 0);

    /* This is freed up by the caller. */
    char *datetime_and_log_number = (char *)malloc(size * sizeof(char));

    char *msg_format = (hasState && !hasRequestedResource) ?  "(%03d): %s" :
                       (hasState &&  hasRequestedResource) ?  "%s(%03d): %s" :
                       "%s";
    char *requested_resource = hasRequestedResource ?
                               image_state->requested_resource : NULL;
    int log_number = hasState ? (image_state->log_number)++ : 0;
    if (hasState && !hasRequestedResource)
    {
        sprintf(datetime_and_log_number, msg_format,
                log_number, datetime);
    }
    else if (hasState &&  hasRequestedResource)
    {
        sprintf(datetime_and_log_number, msg_format,
                requested_resource, log_number, datetime);
    }
    else
    {
        sprintf(datetime_and_log_number, msg_format,
                datetime);
    }
    return datetime_and_log_number;
}

void log_entry_to_file(char* log_msg)
{
#ifndef __WIN32__
    pthread_mutex_lock (&log_mutex);
#endif // __WIN32__

    fprintf(tiff_to_scaled_png_log, log_msg);
    fflush(tiff_to_scaled_png_log);
#ifndef __WIN32__
    pthread_mutex_unlock (&log_mutex);
#endif // __WIN32__
}

void log_entry(const char *msg, image_state_t *image_state)
{
    if (get_debug_log_level() < LOG_LEVEL_DEBUG && get_native_test() == 0)
    {
        return;
    }

    char *datetime_and_log_number =
        get_time_log_number_str(image_state);
    char log_msg[strlen(datetime_and_log_number) + strlen(msg) + 10];
    sprintf(log_msg, "%s: %s\n", datetime_and_log_number, msg);

    free(datetime_and_log_number);

    log_entry_to_file(log_msg);
}

void log_class_entry(const char *msg)
{
    if (get_debug_log_level() < LOG_LEVEL_DEBUG && get_native_test() == 0)
    {
        return;
    }

    char *datetime_and_log_number =
        get_time_log_number_str(NULL);
    char log_msg[strlen(datetime_and_log_number) + strlen(msg) + 10];
    sprintf(log_msg, "%s: %s\n", datetime_and_log_number, msg);

    free(datetime_and_log_number);

    log_entry_to_file(log_msg);
}

void log_class_name(JNIEnv *env, jclass g_u_p_image_State_class)
{
    jclass classMethodAccess = (*env)->FindClass(env, "java/lang/Class");

    jmethodID mid_getName = (*env)->GetMethodID(env, classMethodAccess,
                            "getName", "()Ljava/lang/String;");
    jstring jname = (*env)->CallObjectMethod(env, g_u_p_image_State_class, mid_getName);

    const char *name = (*env)->GetStringUTFChars(env, jname, NULL);

    char msg[255];
    sprintf(msg, "Class name: %s", name);
    log_class_entry(msg);

    (*env)->ReleaseStringUTFChars(env, jname, name);
}

void jni_GetCONVERTER_LEVEL_LOG_from_State(JNIEnv *env, jclass g_u_p_image_State_class)
{
    Debug_Level = (*env)->GetStaticIntField(env, g_u_p_image_State_class,
                                            g_u_p_image_State_Debug_Level_FID);
}

void jni_GetPNG_COMPRESS_LEVEL_from_State(JNIEnv *env, jclass g_u_p_image_State_class)
{
    Png_Compression_Level = (*env)->GetStaticIntField(env, g_u_p_image_State_class,
                                                      g_u_p_image_Png_Compression_Level_FID);
}

/**
 * Only run if the debug level is sufficently high enough.  If
 * the path is missing, punt back to gathering telemetry only.
 */
void jni_GetNATIVE_LOG_PATH_from_State(JNIEnv *env, jclass g_u_p_image_State_class)
{
    if (Debug_Level < LOG_LEVEL_DEBUG)
    {
        return;
    }
    jstring native_log_path =
        (*env)->GetStaticObjectField(env, g_u_p_image_State_class,
                                     g_u_p_image_State_Log_Path_FID);
    int missing_path = native_log_path == NULL;
    if (! missing_path)
    {
        missing_path = (*env)->GetStringLength(env, native_log_path) == 0;
    }
    Debug_Level = missing_path ? LOG_LEVEL_TELEMETRY : Debug_Level;

    Log_Path = (*env)->GetStringUTFChars(env, native_log_path, 0);
}

/**
 * The resource request is the reference to the TIFF file to be converted.
 * It could be a file request or a URI path. It is leveraged in logging
 * and exception tossing.
 */
void jni_GetRequestedResource_from_State(image_state_t* image_state,
        JNIEnv *env, jobject java_state)
{
    jstring j_requested_resource =
        (*env)->GetObjectField(env, java_state,
                               g_u_p_image_State_RequestedResource_FID);

    int missing_requested_resource = j_requested_resource == NULL;
    if (! missing_requested_resource)
    {
        missing_requested_resource =
            (*env)->GetStringLength(env, j_requested_resource) == 0;
    }

    char image_number_str[10];
    if (missing_requested_resource)
    {
        int image_number;

#ifndef __WIN32__
        pthread_mutex_lock (&image_number_mutex);
#endif // __WIN32__

        image_number = Missing_Image_Number++;

#ifndef __WIN32__
        pthread_mutex_unlock (&image_number_mutex);
#endif // __WIN32__

        sprintf(image_number_str, "%05d", image_number);
    }
    const char *requested_resource = missing_requested_resource ? image_number_str :
                                     (*env)->GetStringUTFChars(env, j_requested_resource, 0);

    /* freed up in jni_SetAll_in_State() so it must be called even in exceptional conditions! */
    image_state->requested_resource = (char *)malloc((strlen(requested_resource) + 1) * sizeof(char));

    strcpy(image_state->requested_resource, requested_resource);
    if (! missing_requested_resource)
    {
        (*env)->ReleaseStringUTFChars(env, j_requested_resource, requested_resource);
    }
}

void  jni_GetTiffCompressedSize_in_State(image_state_t* image_state,
        JNIEnv *env, jobject java_state)
{
    image_state->tiff_compressed_size =
        (*env)->GetIntField(env, java_state,
                            g_u_p_image_State_TiffCompressedSize_FID);
}

void jni_GetImageChain_from_State(image_state_t* image_state,
                                  JNIEnv *env, jobject java_state)
{
    image_state->image_chain =
        (*env)->GetIntField(env, java_state,
                            g_u_p_image_State_ImageChain_FID);
}

void jni_GetTiffImage_from_State(image_state_t* image_state,
                                 JNIEnv *env, jobject java_state)
{
    jbyteArray tiffAry =
        (jbyteArray)(*env)->GetObjectField(env, java_state,
                                           g_u_p_image_State_TiffImage_FID);

    jni_GetTiffCompressedSize_in_State(image_state, env, java_state);

    int tiff_compressed_size = image_state->tiff_compressed_size;

    // this buffer is freed up immediately after the TIFF is decompressed.
    image_state->tiff_image = (unsigned char *)malloc(tiff_compressed_size * sizeof(unsigned char));

    (*env)->GetByteArrayRegion(env, tiffAry, 0, tiff_compressed_size,
                               (jbyte *)image_state->tiff_image);
}

void  jni_SetPngImage_in_State(image_state_t* image_state, JNIEnv *env, jobject java_state)
{
    jbyteArray pngAry = (*env)->NewByteArray(env, image_state->png_compressed_size);

    (*env)->SetByteArrayRegion(env, pngAry, 0, image_state->png_compressed_size,
                               (jbyte *)image_state->png_image);
    (*env)->SetObjectField(env, java_state, g_u_p_image_State_PngImage_FID, pngAry);
}

void  jni_SetTiffWidth_in_State(image_state_t* image_state, JNIEnv *env, jobject java_state)
{
    (*env)->SetIntField(env, java_state, g_u_p_image_State_TiffWidth_FID,
                        (jint) image_state->tiff_width);
}

void  jni_SetTiffHeight_in_State(image_state_t* image_state,
                                 JNIEnv *env, jobject java_state)
{
    (*env)->SetIntField(env, java_state, g_u_p_image_State_TiffHeight_FID,
                        (jint) image_state->tiff_height);
}

void  jni_SetImageConvertTime_in_State(image_state_t* image_state,
                                       JNIEnv *env, jobject java_state)
{
    (*env)->SetDoubleField(env, java_state, g_u_p_image_State_ImageConvertTime_FID,
                           (jdouble) image_state->image_convert_time);
}

void  jni_SetImageDecompressTime_in_State(image_state_t* image_state,
        JNIEnv *env, jobject java_state)
{
    (*env)->SetDoubleField(env, java_state, g_u_p_image_State_ImageDecompressTime_FID,
                           (jdouble) image_state->image_decompress_time);
}

void  jni_SetImageScaleTime_in_State(image_state_t* image_state, JNIEnv *env, jobject java_state)
{
    (*env)->SetDoubleField(env, java_state, g_u_p_image_State_ImageScaleTime_FID,
                           (jdouble) image_state->image_scale_time);
}

void  jni_SetImageEncodePngTime_in_State(image_state_t* image_state,
        JNIEnv *env, jobject java_state)
{
    (*env)->SetDoubleField(env, java_state, g_u_p_image_State_ImageEncodePngTime_FID,
                           (jdouble) image_state->image_encode_png_time);
}

void  jni_SetTotalNativeTime_in_State(image_state_t* image_state,
                                      JNIEnv *env, jobject java_state)
{
    (*env)->SetDoubleField(env, java_state, g_u_p_image_State_TotalNativeTime_FID,
                           (jdouble) image_state->total_native_time);
}

void jni_GetPngWidth_from_State(image_state_t* image_state,
                                JNIEnv *env, jobject java_state)
{
    image_state->png_width = (*env)->GetIntField(env, java_state,
                             g_u_p_image_State_PngWidth_FID);
}

void  jni_SetPngWidth_in_State(image_state_t* image_state,
                               JNIEnv *env, jobject java_state)
{
    (*env)->SetIntField(env, java_state,
                        g_u_p_image_State_PngWidth_FID,
                        (jint)image_state->png_width);
}

void  jni_SetPngHeight_in_State(image_state_t* image_state, JNIEnv *env, jobject java_state)
{
    (*env)->SetIntField(env, java_state,
                        g_u_p_image_State_PngHeight_FID,
                        (jint)image_state->png_height);
}

void  jni_SetGpuDevice_in_State(image_state_t* image_state, JNIEnv *env, jobject java_state)
{
    (*env)->SetIntField(env, java_state,
                        g_u_p_image_State_GpuDevice_FID,
                        (jint)image_state->gpu_device);
}

void  jni_SetGpuWaitTime_in_State(image_state_t* image_state,
        JNIEnv *env, jobject java_state)
{
    (*env)->SetDoubleField(env, java_state, g_u_p_image_State_GpuWaitTime_FID,
                           (jdouble) image_state->gpu_wait_time);
}

void init_image_state(image_state_t *image_state)
{
    image_state->requested_resource = NULL;
    image_state->image_chain = 0;
    image_state->tiff_image = NULL;
    image_state->tiff_image_size = 0;
    image_state->tiff_width = 0;
    image_state->tiff_height = 0;
    image_state->tiff_compressed_size = 0;
    image_state->image_convert_time = 0.0f;
    image_state->image_decompress_time = 0.0f;
    image_state->image_scale_time = 0.0f;
    image_state->image_encode_png_time = 0.0f;
    image_state->gpu_wait_time = 0.0f;
    image_state->gpu_device = -1;
    image_state->total_native_time = 0.0f;
    image_state->png_image = NULL;
    image_state->aux_data0 = NULL;
    image_state->aux_data1 = NULL;
    image_state->aux_data2 = NULL;
    image_state->png_compressed_size = 0;
    // Must be assigned in init or exception.
    // image_state->png_width = 0;
    image_state->png_height = 0;
    image_state->png_alloced_size = 0;
    image_state->log_number = 0;
}

/* Transfer input out of Java gov.uspto.pe2e.image.State
   on VM stack into image_state_t on native stack. */
void jni_GetAll_from_State(image_state_t* image_state,
                           JNIEnv* env, jobject java_state)
{
    init_image_state(image_state);

    jni_GetRequestedResource_from_State(image_state, env, java_state);
    jni_GetTiffImage_from_State(image_state, env, java_state);
    jni_GetPngWidth_from_State(image_state, env, java_state);
    jni_GetImageChain_from_State(image_state, env, java_state);
}

/* Transfer output out of image_state_t on native stack
   into Java gov.uspto.pe2e.image.State on VM stack. */
void jni_SetAll_in_State(image_state_t* image_state,
                         JNIEnv* env, jobject java_state)
{
    if (get_debug_log_level() >= LOG_LEVEL_TELEMETRY)
    {
        jni_SetTiffWidth_in_State(image_state, env, java_state);
        jni_SetTiffHeight_in_State(image_state, env, java_state);

        jni_SetImageConvertTime_in_State(image_state, env, java_state);
        jni_SetImageDecompressTime_in_State(image_state, env, java_state);
        jni_SetImageScaleTime_in_State(image_state, env, java_state);
        jni_SetImageEncodePngTime_in_State(image_state, env, java_state);
        jni_SetTotalNativeTime_in_State(image_state, env, java_state);
        jni_SetGpuWaitTime_in_State(image_state, env, java_state);

        jni_SetGpuDevice_in_State(image_state, env, java_state);
        jni_SetPngHeight_in_State(image_state, env, java_state);
    }

    jni_SetPngImage_in_State(image_state, env, java_state);
}

void destroy_state(image_state_t* image_state)
{
    free(image_state->requested_resource);
    image_state->requested_resource = NULL;
    image_state->png_alloced_size = 0;
}

void initLog(JNIEnv *env)
{
    int debug_log_level = get_debug_log_level();
    if (debug_log_level < LOG_LEVEL_DEBUG && get_native_test() == 0)
    {
        return;
    }

    int append_to_log = 1;
    if ( 0 != access( Log_Path, W_OK ) )
    {
        append_to_log = 0;
    }

    tiff_to_scaled_png_log = fopen(Log_Path, "a");

    if (NULL == tiff_to_scaled_png_log)
    {
        char buf[1024];
        sprintf(buf, "Failed to open log file(errno: %s): %s\n",
                strerror(errno), Log_Path);
        printf(buf);
        throw_class_tiff_to_scaled_png_exception(env, buf);
        return;
    }

    log_class_entry("Native code restart.");
    char log_msg[255];
    sprintf(log_msg, "%s log (debug log level %d)",
            (append_to_log == 0 ? "Starting new" : "Appending to"),
            get_debug_log_level());
    log_class_entry(log_msg);
}

JNIEXPORT jint JNICALL Java_com_kedges_tiffscaledpng_State_initStatics(JNIEnv *env,
        jclass g_u_p_image_State_class)
{
    initStateFieldIDs(env, g_u_p_image_State_class);
#ifdef __WIN32__
    init_win32_frequency();
#endif // __WIN32__

    jni_GetCONVERTER_LEVEL_LOG_from_State(env, g_u_p_image_State_class);
    jni_GetPNG_COMPRESS_LEVEL_from_State(env, g_u_p_image_State_class);
    jni_GetNATIVE_LOG_PATH_from_State(env, g_u_p_image_State_class);
    initLog(env);

    /* initLog() function could set an exception state. Check for the
       exceptional state before continuing.
    */
    jthrowable throwObj = (*env)->ExceptionOccurred(env);
    if (throwObj != NULL)
    {
        return 0;
    }

    /* is_nvidia_gpu() function could set an exception state. Check for the
       exceptional state before continuing.

    set_has_gpu(is_nvidia_gpu());
    */
    set_has_gpu(0);

    throwObj = (*env)->ExceptionOccurred(env);
    if (throwObj != NULL)
    {
        return 0;
    }

    return get_has_gpu();
}

void throw_tiff_to_scaled_png_exception(JNIEnv *env, const char *msg,
                                        image_state_t *image_state)
{

    char *datetime_and_log_number =
        get_time_log_number_str(image_state);
    char exception_msg[strlen(datetime_and_log_number) + strlen(msg) + 10];
    sprintf(exception_msg, "%s: %s\n", datetime_and_log_number, msg);

    free(datetime_and_log_number);

    if (env != NULL) {
        log_entry(exception_msg, image_state);
        jclass exceptionClass =
            (*env)->FindClass(env, "gov/uspto/pe2e/image/TiffToScaledPngException");
        (*env)->ThrowNew(env, exceptionClass, exception_msg);
    }
    else {
        log_entry(exception_msg, image_state);
    }
    return;
}

void throw_class_tiff_to_scaled_png_exception(JNIEnv *env, const char *msg)
{
    log_class_entry(msg);

    char *datetime_and_log_number =
        get_time_log_number_str(NULL);
    char exception_msg[strlen(datetime_and_log_number) + strlen(msg) + 10];
    sprintf(exception_msg, "%s: %s\n", datetime_and_log_number, msg);

    free(datetime_and_log_number);

    jclass exceptionClass =
        (*env)->FindClass(env, "gov/uspto/pe2e/image/TiffToScaledPngException");
    (*env)->ThrowNew(env, exceptionClass, exception_msg);
    return;
}
