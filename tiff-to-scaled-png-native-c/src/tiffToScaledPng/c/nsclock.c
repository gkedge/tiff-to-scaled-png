
#include <stdio.h>
#include <string.h>

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309L
#endif // _POSIX_C_SOURCE

#define SECS   0
#define MSECS  1
#define MICRO_SECS 2

#ifdef _WIN32
#include <time.h>

#include<windows.h>

static double frequency = 1.0f;

#else // NOT _WIN32

# ifndef __USE_POSIX199309
  #define __USE_POSIX199309
# endif
#include <unistd.h>
#include <sys/types.h>
#include <time.h>

# ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
# endif

# ifdef CLOCK_MONOTONIC_RAW
#  define NSCLOCK CLOCK_MONOTONIC_RAW
# elif CLOCK_MONOTONIC
#  define NSCLOCK CLOCK_MONOTONIC
# else 
#  define NSCLOCK CLOCK_REALTIME
# endif

#endif // NOT _WIN32

#include "nsclock.h"

static double get_elapsed_time(point_in_time_t, int);

#ifdef __WIN32__
void
init_win32_frequency()
{
    LARGE_INTEGER w_freq;
    QueryPerformanceFrequency(&w_freq);
    frequency = (double)w_freq.QuadPart;
}
#endif // __WIN32__

point_in_time_t get_time_now()
{
#ifdef __WIN32__

    LARGE_INTEGER w_start;
    QueryPerformanceCounter(&w_start);
    point_in_time_t now = w_start.QuadPart;

#else // NOT __WIN32__

    struct timespec now_timespec;
    
# ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    now_timespec.tv_sec = mts.tv_sec;
    now_timespec.tv_nsec = mts.tv_nsec;
# else
    clock_gettime(CLOCK_REALTIME, &now_timespec);
# endif

    point_in_time_t now = (point_in_time_t)now_timespec.tv_sec + 1.0e-9 * now_timespec.tv_nsec;

#endif

    return now;
}

double get_elapsed_time_secs(point_in_time_t start_time)
{
  return get_elapsed_time(start_time, SECS);
}

double get_elapsed_time_ms(point_in_time_t start_time)
{
  return get_elapsed_time(start_time, MSECS);
}

double get_elapsed_time_micro_secs(point_in_time_t start_time)
{
  return get_elapsed_time(start_time, MICRO_SECS) ;
}

// buf needs to store 21 characters
int get_time_stamp(char *buf, int len)
{
  struct tm t;

#ifdef __WIN32__
    time_t long_time;
    time( &long_time );

    struct tm *newtime;
    if ((newtime = localtime( &long_time )) == NULL)
        return 1;

    t = *newtime;

#else // NOT __WIN32__

    struct timespec now_timespec;

# ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    now_timespec.tv_sec = mts.tv_sec;
    now_timespec.tv_nsec = mts.tv_nsec;
# else
    clock_gettime(CLOCK_REALTIME, &now_timespec);
# endif

    long secs = now_timespec.tv_sec;

    if (localtime_r(&(secs), &t) == NULL)
        return 1;

#endif

  int ret = strftime(buf, len, "%m%d:%H:%M:%S", &t);
  if (ret == 0)
    return 2;
  len -= ret - 1;

#ifndef __WIN32__

  ret = snprintf(&buf[strlen(buf)], len, ".%06ld", now_timespec.tv_nsec / 1000L);
  if (ret >= len)
    return 3;

#endif // NOT __WIN32__

  return 0;
}

static double get_elapsed_time(point_in_time_t start_time, int secs_msecs_or_micro_secs)
{
    double elapsed = get_time_now() - start_time;

    if (secs_msecs_or_micro_secs == MSECS) {
        elapsed *= 1000.0f;
    }
    else if (secs_msecs_or_micro_secs == MICRO_SECS) {
        elapsed *= 1000000.0f;
    }

#ifdef _WIN32
    elapsed = elapsed / frequency;
#endif

    return elapsed;
}

// Test code
int nsclock_main()
{
#ifdef __CYGWIN__
    printf("__CYGWIN__\n");
#endif

#ifdef __MINGW32__
    printf("__MINGW32__\n");
#endif

#ifdef __MINGW64__
    printf("__MINGW64__\n");
#endif

#ifdef __USE_POSIX199309
    printf("__USE_POSIX199309\n");
#endif

#ifdef __WIN32__
    printf("__WIN32__\n");

    init_win32_frequency();

#else // NOT __WIN32__

#endif // __WIN32__

    char datetime[25];
    get_time_stamp(datetime, sizeof(datetime));

    printf("nsclock start: %s\n", datetime);

    point_in_time_t start_time = get_time_now();

    printf("nsclock wait\n");
    int ii = 0;
    int max = 10000;
    for (ii = 0; ii < max; ii++)
    {
        printf(".");
        fflush(stdout);
    }

    printf("\nnsclock done waiting.\n");

    double elapsed = get_elapsed_time_secs(start_time);
    printf("%.02fsecs.\n", elapsed);

    elapsed = get_elapsed_time_ms(start_time);
    printf("%.02fmsecs.\n", elapsed);

    elapsed = get_elapsed_time_micro_secs(start_time);
    printf("%.02fmicro_secs.\n", elapsed);

    printf("nsclock done.\n");

#ifdef __WIN32__

    Sleep(5000);

#else // NOT __WIN32__

    sleep(5);

#endif // __WIN32__
    return 0;
}
