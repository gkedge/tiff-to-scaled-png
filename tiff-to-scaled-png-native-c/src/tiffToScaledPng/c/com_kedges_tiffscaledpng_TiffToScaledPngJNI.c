#include <stdlib.h>

#include "nsclock.h"
#include "state.h"
#include "tiff-to-scaled-png.h"

void log_tiff_to_scaled_png_class(JNIEnv *env, jclass tiff_to_scale_png_class);

JNIEXPORT void JNICALL Java_com_kedges_tiffscaledpng_TiffToScaledPngJNI_convertTiffToScaledPngJNI
(JNIEnv *env, jclass tiff_to_scale_png_class, jobject java_state)
{
    struct State image_state;
    int debug_log_level = get_debug_log_level();

    /* jni_GetAll_from_State converts all input state data from a
       the Java stack contained within gov.uspto.pe2e.image.State to the
       C stack contained within image_state_t (jniState.h).
    */
    jni_GetAll_from_State(&image_state, env, java_state);

    point_in_time_t total_native_time_start = 0;
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        total_native_time_start = get_time_now();
    }

    if (debug_log_level >= LOG_LEVEL_DEBUG)
    {
        log_entry("convertTiffToScaledPngJNI: Starting.",
                  &image_state);
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_class_name(env, tiff_to_scale_png_class);
        }
    }

    if (image_state.tiff_image == NULL || image_state.png_width < 2)
    {
        const char *msg = image_state.tiff_image == NULL ?
                          "convertTiffToScaledPngJNI: No tiff image to convert." :
                          "convertTiffToScaledPngJNI:PNG width < 2px.";

        throw_tiff_to_scaled_png_exception(env, msg, &image_state);

        return;
    }


    switch (image_state.image_chain)
    {
    case OPENCV_CONVERSION_PATH:
    case OPENCV_CUDA_CONVERSION_PATH:
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("convertTiffToScaledPngJNI: calling convert_tiff_to_scaled_png_opencv()",
            &image_state);
        }
        convert_tiff_to_scaled_png_opencv(env, &image_state);
        break;
    case LEPTONICA_CONVERSION_PATH:
    case LEPTONICA_CUDA_CONVERSION_PATH:
    case LEPTONICA_HYBRID_CONVERSION_PATH:
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("convertTiffToScaledPngJNI: calling convert_tiff_to_scaled_png_leptonica()",
            &image_state);
        }
        convert_tiff_to_scaled_png_leptonica(env, &image_state);
        break;
    }

    /* CAREFUL!  That convert_tiff_to_scaled_png() function could
       set an exception state.  At this point, I want the following
       operations to continue.  But, if more processing would be
       compromised in an exceptional situation, this code path
       may have to check for the exceptional state before continuing.

       jthrowable throwObj = (*env)->ExceptionOccurred(env);

       if (throwObj != NULL) {
          return;
       }

       NOTE: Not calling destroy_state() or destroy_cv_image_chain, even
       in exceptional conditions will result in memory leaks!
    */
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("convertTiffToScaledPngJNI: returned from convert_tiff_to_scaled_png()",
                  &image_state);
    }

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state.total_native_time =
            get_elapsed_time_ms(total_native_time_start);
    }

    /* jni_SetAll_in_State converts all output state data from the
       C stack contained within image_state_t (jniState.h)
       to the Java stack, contained within a gov.uspto.pe2e.image.State.
    */
    jni_SetAll_in_State(&image_state, env, java_state);

    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("convertTiffToScaledPngJNI: calling imaging chain destroy",
                  &image_state);
    }

    // In non-test situations, the tiff image is freed immediately
    // after it is decompressed.
    if (get_native_test() == 1) {
        free(image_state.tiff_image);
    }

    if (image_state.image_chain == OPENCV_CONVERSION_PATH ||
        image_state.image_chain == OPENCV_CUDA_CONVERSION_PATH)
    {
        destroy_opencv_image_chain(&image_state);
    }

    if (debug_log_level >= LOG_LEVEL_DEBUG)
    {
        log_entry("convertTiffToScaledPngJNI: returning",
                  &image_state);
    }

    destroy_state(&image_state);
    return;
}


