/*
 * All OpenCV specific code is concentrated within this single file. Should
 * interest be extended to consider other native image libraries, implement
 * the signatures declared in tiff-to-scaled-png.h in a separate library-specific
 * source file.  Add another shared library target to the build system
 * that is library-specific.
 */
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "nsclock.h"
#include "tiff-to-scaled-png.h"

static IplImage *decompress_tiff_opencv(JNIEnv *env, image_state_t *image_state);
static IplImage *scale_image_opencv(JNIEnv *env, image_state_t *image_state,
                             IplImage *source, int dest_width, int dest_height);
static void compress_png_opencv(JNIEnv *env, image_state_t *image_state, IplImage *source);

/*
 * For gray scale text images, not seeing any difference in
 * image size over compression values 1-9 inclusive.
 */
static int PNG_COMPRESSION_LEVEL = -1;

void convert_tiff_to_scaled_png_opencv(JNIEnv *env, image_state_t *image_state)
{
    point_in_time_t start_time = 0;
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
        if (debug_log_level >= LOG_LEVEL_DEBUG)
        {
            log_entry("convert_tiff_to_scaled_png_opencv: starting OpenCV imaging chain",
                      image_state);
        }
    }

    IplImage *source = decompress_tiff_opencv(env, image_state);
    if (get_native_test() != 1 && (*env)->ExceptionOccurred(env) != NULL)
    {
        return;
    }

    /* Compute height without skewing the aspect ratio between source and destination image */
    int png_width = image_state->png_width;
    int png_height = (png_width * (source->height)) / (source->width);

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->png_height = png_height;
    }

    IplImage * scaleDest = source;
    if (source->width != png_width)
    {
        scaleDest =
            scale_image_opencv(env, image_state, source, png_width, png_height);
        if (get_native_test() != 1 && (*env)->ExceptionOccurred(env) != NULL)
        {
            return;
        }
    }

    compress_png_opencv(env, image_state, scaleDest);
    if (get_native_test() != 1 && (*env)->ExceptionOccurred(env) != NULL)
    {
        return;
    }

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_convert_time =
            get_elapsed_time_ms(start_time);
    }

    /* The png_image MAT will be freed at the end of
     * Java_com_kedges_tiffscaledpng_TiffToScaledPngJNI_convertTiffToScaledPng.
     */
    cvReleaseImage(&scaleDest);

    if (debug_log_level >= LOG_LEVEL_DEBUG)
    {
        log_entry("convert_tiff_to_scaled_png_opencv: returning from OpenCV imaging chain",
                  image_state);
    }
    return;
}

static IplImage *decompress_tiff_opencv(JNIEnv *env, image_state_t *image_state)
{
    point_in_time_t start_time = 0;
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
    }

    /* The obtained TIFF image is expected to be a single band of 8bit gray scale data. */
    unsigned char *tiff_image = image_state->tiff_image;
    CvMat cm = cvMat(1, image_state->tiff_compressed_size,
                     CV_8UC1, tiff_image);
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        unsigned char *internal_image_chars = image_state->tiff_image;
        char byte_order = internal_image_chars[0];
        int motorola_big_endian = byte_order == 'M';
        int intel_little_endian = byte_order == 'I';
        unsigned int directoryAddress = ((internal_image_chars[motorola_big_endian ? 4 : 7]) << 24) |
                                        ((internal_image_chars[motorola_big_endian ? 5 : 6]) << 16) |
                                        ((internal_image_chars[motorola_big_endian ? 6 : 5]) << 8) |
                                        internal_image_chars[motorola_big_endian ? 7 : 4];
        // A TIFF image signature (or magic number) should either be:
        // II/2A/directory or MM/00/directory
        char signature_buf[strlen("TIFF: Size: -2147483647; Signature: [II\0042\0x00000000\0000000000]") + 100];
        sprintf(signature_buf,
                "TIFF: Size: %d; Signature: [%c%c\\%02d%02d\\0x%x\\%u]",
                image_state->tiff_compressed_size,
                internal_image_chars[0], internal_image_chars[1],
                internal_image_chars[motorola_big_endian ? 2 : 3],
                internal_image_chars[motorola_big_endian ? 3 : 2],
                directoryAddress, directoryAddress);
        char msg[] = "decompress_tiff_opencv:: starting TIFF decompression:";
        char log_msg[strlen(msg) + strlen(signature_buf) + 10];
        sprintf(log_msg, "%s %s", msg, signature_buf);
        log_entry(log_msg, image_state);
    }
    IplImage *source = cvDecodeImage(&cm, CV_LOAD_IMAGE_GRAYSCALE);

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_decompress_time = get_elapsed_time_ms(start_time);
        image_state->tiff_width = source->width;
        image_state->tiff_height = source->height;
    }

    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("decompress_tiff_opencv: decompressed TIFF",
                  image_state);
    }

    /* Liberate the TIFF image data; we are done with it. */
    if (get_native_test() == 0) {
        free(image_state->tiff_image);
    }
    if (NULL == source)
    {
        const char *msg = "decompress_tiff_opencv: in-memory image decode failed.";
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return NULL;
    }

    return source;
}

static IplImage *scale_image_opencv(JNIEnv *env, image_state_t *image_state,
                                    IplImage *source, int dest_width, int dest_height)
{
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("scale_image_opencv: starting image scaling",
                  image_state);
    }

    /* Create a new OpenCV image to hold the scale version */
    IplImage *scaleDest = cvCreateImage(cvSize(dest_width, dest_height),
                                        source->depth, source->nChannels );
    if (NULL == scaleDest)
    {
        const char *msg = "scale_image_opencv: opencv failed to create scale image.";
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return NULL;
    }

    /* Generally, it is best to enlarge images using over
     * a 4x4 or 8x8 pixel neighbor, but with text images,
     * it doesn't seem to add much to beyond simple biliner
     * interpolation.  This may change for drawings or images
     * but that will have to be better tested. */
    int interpolation = source->width > image_state->png_width ?
                        CV_INTER_AREA : CV_INTER_LINEAR;

    point_in_time_t start_time = 0;
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
    }
    /* This function performs the actual resize/scale operation */
    cvResize(source, scaleDest, interpolation);
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_scale_time = get_elapsed_time_ms(start_time);
    }

    cvReleaseImage(&source);

    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("scale_image_opencv: finished image scaling",
                  image_state);
    }

    return scaleDest;
}

static void compress_png_opencv(JNIEnv *env, image_state_t *image_state, IplImage *source)
{
    point_in_time_t start_time = 0;
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("compress_png_opencv: starting PNG encoding",
                      image_state);
        }
    }

    if (PNG_COMPRESSION_LEVEL == -1) {
        PNG_COMPRESSION_LEVEL = get_png_compression_level();
    }
    int PNG_COMPRESSIONS_PARAMS[3] =
        {CV_IMWRITE_PNG_COMPRESSION, PNG_COMPRESSION_LEVEL, 0};

    CvMat *png_Mat = cvEncodeImage(".png", source, PNG_COMPRESSIONS_PARAMS);
    if (NULL == png_Mat)
    {
        const char *msg = "compress_png_opencv: opencv failed to encode PNG image.";
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return;
    }

    /* This mat is released after the PNG data is copied back
       onto the Java stack.  destroy_cv_image_chain() at the bottom of
       this source file will take care of that. It is called
       at the conclusion of conversion operations. grep(1) for it.
     */
    image_state->aux_data0 = (long long *)png_Mat;
    // Never free the png_image! Rely on destroy_cv_image_chain()
    // to clean up the Mat AND it's contents.
    image_state->png_image = png_Mat->data.ptr;
    image_state->png_compressed_size = png_Mat->cols;

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_encode_png_time = get_elapsed_time_ms(start_time);
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("compress_png_opencv: finished PNG encoding",
                      image_state);
        }
    }
}

void destroy_opencv_image_chain(image_state_t *image_state)
{
    if (image_state->aux_data0 != NULL)
    {
        if (get_debug_log_level() >= LOG_LEVEL_TRACE)
        {
            log_entry("destroy_opencv_image_chain: releasing PNG compression MAT",
                      image_state);
        }
        cvReleaseMat((CvMat **)&(image_state->aux_data0));
        image_state->png_image = NULL;
    }
}

