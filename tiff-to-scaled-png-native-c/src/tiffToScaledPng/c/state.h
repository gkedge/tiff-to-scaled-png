#ifndef _State
#define _State

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * This header is the interface to associated with the 'state' of
 * a converstion. The 'state' is the data that is shared between
 * a Java-based request and the 'C' native code.
 *
 * 'State' is represented on the Java stack by an intance of
 * gov.uspto.pe2e.image.State. 'State' on the 'C' stack
 * is represented by the State struct defined in below in this
 * header.
 *
 * 'State' is a 'handle' to all data associated with a single
 * conversion and holds all input to the native conversion and
 * all output of the conversion.
 *
 * When 'state' moves across the Java/native boundry, the
 * gov.uspto.pe2e.image.State instance is copied into the
 * State struct.
 *
 * When the conversion is finished, output held in the State
 * struct is reflectively copied into the original
 * gov.uspto.pe2e.image.State instance received at the start.
 * The calling methods can then refer to the original
 * gov.uspto.pe2e.image.State instance for the converted
 * image and any telemetry data gathered.
 */

#define LOG_LEVEL_NONE 0
#define LOG_LEVEL_TELEMETRY 1
#define LOG_LEVEL_DEBUG 2
#define LOG_LEVEL_TRACE 3

#define OPENCV_CONVERSION_PATH 0
#define OPENCV_CUDA_CONVERSION_PATH 1
#define LEPTONICA_CONVERSION_PATH 2
#define LEPTONICA_CUDA_CONVERSION_PATH 3
#define LEPTONICA_HYBRID_CONVERSION_PATH 4

/* This structure contains the 'state' data on the 'C' stack.
 *
 * It holds the input & output for the native call to convert
 * TIFF's to scaled PNGs and telemetry related data.
 *
 * For this native implementation, careful attention has been
 * made to thread-safety by leveraging the stack for all data.
 * Only log file support is static across invocations and hence,
 * only a very few operations related to logging need to be
 * synchronized across contending threads.
 *
 * So, all data that pertains to a single conversion are held
 * within this State struct created on the requesting thread's
 * stack and populated with the input content of the passed
 * Java gov.uspto.pe2e.image.State object.
 *
 * The same instance of the Java gov.uspto.pe2e.image.State object
 * is reflectively updated with the conversion results and any
 * telemetry associated with that conversion.
 */
struct State
{
  // Input ...
  char *requested_resource;
  unsigned char *tiff_image;
  int png_width;
  int image_chain;

  // Derived input
  int tiff_compressed_size;

  // Output...
  unsigned char *png_image;

  // Telemetry
  int tiff_image_size;

  int tiff_width;
  int tiff_height;

  double total_native_time;
  double image_convert_time;
  double image_decompress_time;
  double image_scale_time;
  double image_encode_png_time;

  double gpu_wait_time;
  int gpu_device;

  int png_compressed_size;

  int png_height;


  // Data shared among native functions during the processing of
  // a single operation.
  long png_alloced_size;
  int log_number;
  long long *aux_data0;
  long long *aux_data1;
  long long *aux_data2;
};
typedef struct State image_state_t;

/**
 * jni_GetAll_from_State is called at the start of conversion.
 * It reflectively reads the passed Java gov.uspto.pe2e.image.State
 * object for input data needed for the conversion.
 */
void jni_GetAll_from_State(struct State*, JNIEnv*, jobject);

/**
 * jni_SetAll_in_State is called at the end of conversion.
 * It reflectively updates the original instance of the
 * passed Java gov.uspto.pe2e.image.State object.
 */
void jni_SetAll_in_State(struct State*, JNIEnv*, jobject);

/**
 * destroy_state is called at the very, bloody end to clean
 * up state data that was allocated on the heap.
 */
void destroy_state(image_state_t *) ;

int get_debug_log_level();
void set_debug_log_level(int);
int get_native_test();
void set_native_test(int);

int is_nvidia_gpu();
int get_has_gpu();
void set_has_gpu(int);

int get_image_conversion_path();
int get_png_compression_level();

/* Logging and exception tossing support for operations that have
 * state and those that are related to static operations.
 */
void initLog(JNIEnv *env);
void log_class_name(JNIEnv *env, jclass clazz);
void log_class_entry(const char *msg);
void log_entry(const char *msg, image_state_t *image_state);
void throw_class_tiff_to_scaled_png_exception(JNIEnv *env, const char *msg);
void throw_tiff_to_scaled_png_exception(JNIEnv *, const char *, image_state_t *);

void init_image_state(image_state_t *image_state);

#ifdef __cplusplus
}
#endif

#endif
