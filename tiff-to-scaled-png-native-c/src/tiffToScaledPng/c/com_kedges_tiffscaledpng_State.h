#ifndef _Included_com_kedges_tiffscaledpng_State
#define _Included_com_kedges_tiffscaledpng_State

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     com_kedges_tiffscaledpng_State
 * Method:    initFIDs
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_com_kedges_tiffscaledpng_State_initStatics
  (JNIEnv *, jclass );

#ifdef __cplusplus
}
#endif

#endif
