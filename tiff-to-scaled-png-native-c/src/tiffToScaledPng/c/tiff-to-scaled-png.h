#ifndef tiff_to_scaled_png_h
#define tiff_to_scaled_png_h

#include <jni.h>
#include "state.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 *  These are the methods that wrap around a 3rd party library.
 *  They are expected to implement decompressing of a TIFF,
 *  scaling and compress to a PNG.
 */
void convert_tiff_to_scaled_png_leptonica(JNIEnv *, image_state_t *);
void set_png_compression_level_leptonica(int pngCompressionLevel);

void convert_tiff_to_scaled_png_opencv(JNIEnv *, image_state_t *);
void set_png_compression_level_opencv(int pngCompressionLevel);
void destroy_opencv_image_chain(image_state_t *);
#ifdef __cplusplus
}
#endif

#endif // tiff_to_scaled_png_h
