#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "nsclock.h"
#include "tiff-to-scaled-png.h"

static const char *IMAGE_PATH_ROOT = "";

int get_image_data_from_tiff_file(image_state_t *image_state)
{
    image_state->tiff_image = NULL;
    image_state->tiff_image_size = 0;

    const char *imagePath = "TestImage10.tif";
    // const char *imagePath = "10by15HorizontalAlternating.tif";

    char fullImagePath[PATH_MAX];
    strcpy(fullImagePath, IMAGE_PATH_ROOT);
    strcat(fullImagePath, imagePath);

    if ( 0 != access( fullImagePath, R_OK ) )
    {
        log_entry("get_image_data_from_tiff_file: failed to access TIFF file.",
                  image_state);
        return 1;
    }


    FILE *tiff_image_file = fopen(fullImagePath, "rb");
    fseek(tiff_image_file, 0L, SEEK_END);
    image_state->tiff_compressed_size = ftell(tiff_image_file);

    char buf[1024];
    sprintf(buf, "Getting gray scale image(%d bytes): %s",
            image_state->tiff_compressed_size, fullImagePath);
    log_entry(buf, image_state);

    /* reset the file position indicator to the beginning of the file */
    fseek(tiff_image_file, 0L, SEEK_SET);

    image_state->tiff_image = (unsigned char*)calloc(image_state->tiff_compressed_size, sizeof(unsigned char));

    if(image_state->tiff_image == NULL)
    {
        log_entry("get_image_data_from_tiff_file: memory error allocating space for tiff image.",
                  image_state);
        return 1;
    }

    /* copy TIFF image into the buffer */
    fread(image_state->tiff_image, sizeof(unsigned char), image_state->tiff_compressed_size, tiff_image_file);
    fclose(tiff_image_file);

    return 0;
}

int set_image_data_to_png_file(image_state_t *image_state, const char *suffix)
{
    if (image_state->png_image == NULL || image_state->png_compressed_size <= 0)
    {
        log_entry("set_image_data_to_png_file: missing png image.",
                  image_state);
        return 1;
    }

    const char *imagePath = "Test";

    char fullImagePath[PATH_MAX];
    strcpy(fullImagePath, IMAGE_PATH_ROOT);
    strcat(fullImagePath, imagePath);
    strcat(fullImagePath, suffix);
    strcat(fullImagePath, ".png");

    FILE *png_image_file = fopen(fullImagePath, "wb");

    /* copy TIFF image into the buffer */
    fwrite(image_state->png_image, sizeof(unsigned char),
           image_state->png_compressed_size, png_image_file);
    fflush(png_image_file);
    fclose(png_image_file);

    char buf[1024];
    sprintf(buf, "Saved gray scale image: %s", fullImagePath);
    log_entry(buf, image_state);

    return 0;
}

int main()
{
    set_debug_log_level(LOG_LEVEL_TRACE);
    set_native_test(1);
    printf("Hello world!\n");

#ifdef __WIN32__
    init_win32_frequency();
#endif // __WIN32__

    int size = 816; // * 0.75f; // 2550 * 0.5;

    JNIEnv *env = NULL;
    initLog(env);
    /* set_has_gpu(is_nvidia_gpu()); */
    set_has_gpu(0);

    image_state_t opencv_image_state;
    init_image_state(&opencv_image_state);
    log_entry("Start OpenCV.", &opencv_image_state);

    int status = get_image_data_from_tiff_file(&opencv_image_state);
    if (status != 0)
    {
       log_entry("Failed to bring in TIFF file.", &opencv_image_state);
       return 1;
    }
    opencv_image_state.png_width = size;

    convert_tiff_to_scaled_png_opencv(env, &opencv_image_state);

    status = set_image_data_to_png_file(&opencv_image_state, "CV");
    if (status != 0)
    {
       log_entry("Failed to save OpenCV PNG file.", &opencv_image_state);
       return 1;
    }

// ----------------------- New ----------------------------------
    image_state_t lept_image_state;
    init_image_state(&lept_image_state);
    log_entry("Start Leptonica.", &lept_image_state);
    lept_image_state.png_width = size;
    int ii = 0;
    for (ii = 0; ii < 20; ii++)
    {
        status = get_image_data_from_tiff_file(&lept_image_state);
        if (status != 0)
        {
            log_entry("Failed to bring in TIFF file.", &lept_image_state);
            return 1;
        }

        lept_image_state.image_chain = LEPTONICA_CONVERSION_PATH;
        convert_tiff_to_scaled_png_leptonica(env, &lept_image_state);
    }
    status = set_image_data_to_png_file(&lept_image_state, "L");
    if (status != 0)
    {
       log_entry("Failed to save Leptonica PNG file.", &lept_image_state);
       return 1;
    }

// ----------- Done.  Report telemetry & clean house. ------------

    log_entry("\n", &lept_image_state);
    char buf[strlen("OpenCV:\t000.00ms;\tLeptonica:\t000.00ms") + 1];
    sprintf(buf,
            "OpenCV: %.02fms;\tLeptonica: %.02fms",
            opencv_image_state.image_decompress_time,
            lept_image_state.image_decompress_time);
    char log_decompress_msg[strlen("Decomp(TIFF):\t") + strlen(buf) + 10];
    sprintf(log_decompress_msg, "Decomp(TIFF):\t%s", buf);
    log_entry(log_decompress_msg, &lept_image_state);

    sprintf(buf,
            "OpenCV: %.02fms;\tLeptonica: %.02fms",
            opencv_image_state.image_scale_time,
            lept_image_state.image_scale_time);
    char log_scale_msg[strlen("Scale:\t\t") + strlen(buf) + 10];
    sprintf(log_scale_msg, "Scale:\t\t%s", buf);
    log_entry(log_scale_msg, &lept_image_state);

    sprintf(buf,
            "OpenCV: %.02fms;\tLeptonical: %.02fms",
            opencv_image_state.image_encode_png_time,
            lept_image_state.image_encode_png_time);
    char log_compress_msg[strlen("Comp(PNG):\t") + strlen(buf) + 10];
    sprintf(log_compress_msg, "Comp(PNG):\t%s", buf);
    log_entry(log_compress_msg, &lept_image_state);

    sprintf(buf,
            "OpenCV: %.02fms;\tLeptonica: %.02fms",
            opencv_image_state.image_convert_time,
            lept_image_state.image_convert_time);
    char log_total_msg[strlen("Total:\t\t") + strlen(buf) + 10];
    sprintf(log_total_msg, "Total:\t\t%s", buf);
    log_entry(log_total_msg, &lept_image_state);

    log_entry("\n", &lept_image_state);

    destroy_opencv_image_chain(&opencv_image_state);
//    destroy_state(&opencv_image_state);
    if (opencv_image_state.png_image)
        free(opencv_image_state.png_image);

    destroy_state(&lept_image_state);
    if (lept_image_state.png_image)
        free(lept_image_state.png_image);

    log_entry("Done.", &lept_image_state);

    return 0;
}

