#ifndef __WIN32__
#include <cuda.h>
#include <npp.h>
#endif // __WIN32__

#include <string.h>
#include <allheaders.h>
#include <semaphore.h>
#include <errno.h>

#include "fifo.h"
#include "state.h"
#include "nsclock.h"

static int devices[] = {0, 1};
static fifo_t *available_devices;
static sem_t device_semaphore;
static sem_t device_count_semaphore;

int is_nvidia_gpu(JNIEnv *env) {
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("is_nvidia_gpu: called", NULL);
    }

    int count = 2;
#ifndef __WIN32__
    NppGpuComputeCapability computeCap = nppGetGpuComputeCapability();
    if (computeCap < NPP_CUDA_2_0)
    {
        const char *msg = "is_nvidia_gpu: no NVidia GPU card.";
        throw_tiff_to_scaled_png_exception(env, msg, NULL);
        return 0;
    }
    else {
        cudaDeviceReset();
        const NppLibraryVersion *nppVersion = nppGetLibVersion();
        cudaGetDeviceCount (&count);
        char log_cuda_info_msg[strlen("Reset/GPU Version/Compute capacity/Device count: 5.5.220/07/2/Tesla K10.G2.8GB") + 20];
        sprintf(log_cuda_info_msg, "Reset/GPU Version/Compute capacity/Device count: %d.%d.%d/%d/%d/%s",
                nppVersion->major, nppVersion->minor, nppVersion->build,
                computeCap, count, nppGetGpuName());
        log_entry(log_cuda_info_msg, NULL);

        sem_init(&device_semaphore, 0, 1);
        sem_init(&device_count_semaphore, 0, count);

        available_devices = fifo_new();
        fifo_add(available_devices, &(devices[0]));
        fifo_add(available_devices, &(devices[1]));
        return 1;
    }
#endif // __WIN32__
    return 0;
}

int isGPUDeviceAvailable() {
    int status = sem_trywait(&device_count_semaphore);
    int available = 0 == status;
    if (available) {
        // if available, bump the count back up that was
        // successfully decremented in the wait.
        sem_post(&device_count_semaphore);
    }
    return available;
}

PIX *scale_image_leptonica_cuda(JNIEnv *env, image_state_t *image_state,
                                PIX *source, int dest_width, int dest_height)
{
    PIX *scaled = source;
#ifndef __WIN32__
    int debug_log_level = get_debug_log_level();
    if (debug_log_level >= LOG_LEVEL_TRACE)
    {
        log_entry("scale_image_leptonica_cuda: starting image scaling",
                  image_state);
    }

    int *device_ptr = NULL;

    char *error_msg_format = "scale_image_leptonica_cuda: %s: CUDA return error: %d";
    char log_error_msg[strlen(error_msg_format) + 80];

    point_in_time_t start_wait_time = 0;
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_wait_time = get_time_now();
    }

    int status = sem_wait(&device_count_semaphore);
    if (status != CUDA_SUCCESS) {
        sprintf(log_error_msg, error_msg_format, "sem_wait error:", errno);
        sem_post(&device_semaphore);
        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
        return NULL;
    }
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->gpu_wait_time =
            get_elapsed_time_ms(start_wait_time);
    }

    sem_wait(&device_semaphore);
    {
        device_ptr = (int *)fifo_remove(available_devices);
    }
    sem_post(&device_semaphore);

    image_state->gpu_device = *device_ptr;

    if (debug_log_level >= LOG_LEVEL_DEBUG)
    {
        char log_semaphore_msg[strlen("scale_image_leptonica_cuda: device: %d.") + 10];
        sprintf(log_semaphore_msg, "scale_image_leptonica_cuda: device: %d.", *device_ptr);
        log_entry(log_semaphore_msg, image_state);
    }

    int error = cudaSetDevice(*device_ptr);
    if (error != CUDA_SUCCESS)
    {
        sprintf(log_error_msg, error_msg_format, "set CUDA device", errno);
        sem_wait(&device_semaphore);
        {
            fifo_add(available_devices, device_ptr);
        }
        sem_post(&device_semaphore);
        sem_post(&device_count_semaphore);
        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
        return NULL;
    }

    point_in_time_t start_time = 0;
    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        start_time = get_time_now();
    }

    PIX *source8 = pixConvert1To8(NULL, source, 255, 0);
//    pixSetPadBits(source8, 0);

    long v = 0x04030201;
    int endian = *((unsigned char *)(&v)) == 0x04 ? 1 : 0;

    if (endian == 0) {
        if (debug_log_level >= LOG_LEVEL_DEBUG)
        {
            log_entry("Swapping before cuda scale...", image_state);
        }
        pixEndianByteSwap(source8);
    }

    int device_src_step;
    Npp8u *pDeviceSrc = nppiMalloc_8u_C1(source8->w, source8->h, &device_src_step);

    int device_dest_step;
    Npp8u *pDeviceDst = nppiMalloc_8u_C1(dest_width, dest_height, &device_dest_step);

    error = cudaMemcpy2D(pDeviceSrc, device_src_step,
                         source8->data, source8->wpl * 4,
                         source8->w, source8->h,
                         cudaMemcpyHostToDevice);
    if (error != CUDA_SUCCESS)
    {
        sprintf(log_error_msg, error_msg_format,
                "copy image from host to device", errno);
        nppiFree(pDeviceSrc);
        nppiFree(pDeviceDst);
        pixDestroy(&source8);
        sem_wait(&device_semaphore);
        {
            fifo_add(available_devices, device_ptr);
        }
        sem_post(&device_semaphore);
        sem_post(&device_count_semaphore);
        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
        return NULL;
    }
    NppiSize device_src_sz;
    device_src_sz.width  = source8->w;
    device_src_sz.height = source8->h;

    NppiRect device_src_roi;
    device_src_roi.x = device_src_roi.y = 0;
    device_src_roi.width  = source8->w;
    device_src_roi.height = source8->h;

    float scale_factor_width = dest_width/(float)source8->w;
    float scale_factor_height = dest_height/(float)source8->h;

    pixDestroy(&source8);

    if (debug_log_level >= LOG_LEVEL_TRACE) {
        char log_scale_msg[strlen("scale_image_leptonica_cuda: scale factor: 0.00000000000000(w) x 0.00000000000000(h)") + 10];
        sprintf(log_scale_msg, "scale_image_leptonica_cuda: scale factor: %f x %f",
                scale_factor_width, scale_factor_height);
        log_entry(log_scale_msg, image_state);
    }
    int interpolation = scale_factor_width < .9f ? NPPI_INTER_SUPER :  NPPI_INTER_LINEAR;

    NppiRect device_dest_roi;
    device_dest_roi.x = device_dest_roi.y = 0;
    device_dest_roi.width  = dest_width;
    device_dest_roi.height = dest_height;

    NppiSize des_sz;
    des_sz.width  = dest_width;
    des_sz.height = dest_height;

//    error = nppiSet_8u_C1R(0, pDeviceDst, device_dest_step, des_sz);
//    if (error != CUDA_SUCCESS)
//    {
//        sprintf(log_error_msg, error_msg_format, "set destination device ROI", error);
//        nppiFree(pDeviceSrc);
//        nppiFree(pDeviceDst);
//        sem_post(&device_count_semaphore);
//        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
//        return NULL;
//    }

    error = nppiResizeSqrPixel_8u_C1R(pDeviceSrc, device_src_sz, device_src_step, device_src_roi,
                                      pDeviceDst, device_dest_step, device_dest_roi,
                                      scale_factor_width, scale_factor_height,
                                      0.0f, 0.0f, interpolation);
    if (error != CUDA_SUCCESS)
    {
        sprintf(log_error_msg, error_msg_format, "resize", errno);
        nppiFree(pDeviceSrc);
        nppiFree(pDeviceDst);
        sem_wait(&device_semaphore);
        {
            fifo_add(available_devices, device_ptr);
        }
        sem_post(&device_semaphore);
        sem_post(&device_count_semaphore);
        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
        return NULL;
    }
    scaled = pixCreate(dest_width, dest_height, 8);
    // memset(scaled->data, 0, 4 * scaled->wpl * scaled->h);

    if (NULL == scaled)
    {
        const char *msg = "scale_image_leptonica_cuda: failed to create scale image.";
        nppiFree(pDeviceSrc);
        nppiFree(pDeviceDst);
        sem_wait(&device_semaphore);
        {
            fifo_add(available_devices, device_ptr);
        }
        sem_post(&device_semaphore);
        sem_post(&device_count_semaphore);
        throw_tiff_to_scaled_png_exception(env, msg, image_state);
        return NULL;
    }

//    error = cudaDeviceSynchronize();
//    if (error != CUDA_SUCCESS)
//    {
//        sprintf(log_error_msg, "scale_image_leptonica_cuda:: %s: CUDA device sync: %d",
//        "device synchronize", error);
//        nppiFree(pDeviceSrc);
//        nppiFree(pDeviceDst);
//        pixDestroy(&scaled);
//        sem_post(&device_count_semaphore);
//        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
//        return NULL;
//    }

    // Copy memory from device to host
    error = cudaMemcpy2D(scaled->data, scaled->wpl * 4, pDeviceDst, device_dest_step,
                         des_sz.width, des_sz.height, cudaMemcpyDeviceToHost);
    if (error != CUDA_SUCCESS)
    {
        sprintf(log_error_msg, error_msg_format, "copy memory from device to host", errno);
        nppiFree(pDeviceSrc);
        nppiFree(pDeviceDst);
        pixDestroy(&scaled);
        sem_wait(&device_semaphore);
        {
            fifo_add(available_devices, device_ptr);
        }
        sem_post(&device_semaphore);
        sem_post(&device_count_semaphore);
        throw_tiff_to_scaled_png_exception(env, log_error_msg, image_state);
        return NULL;
    }

    if (endian == 0) {
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("Swapping back after cuda scale...", image_state);
        }
        pixEndianByteSwap(scaled);
    }

    nppiFree(pDeviceSrc);
    nppiFree(pDeviceDst);

    sem_wait(&device_semaphore);
    {
        fifo_add(available_devices, device_ptr);
    }
    sem_post(&device_semaphore);

    sem_post(&device_count_semaphore);

    if (debug_log_level >= LOG_LEVEL_TELEMETRY)
    {
        image_state->image_scale_time = get_elapsed_time_ms(start_time);
        if (debug_log_level >= LOG_LEVEL_TRACE)
        {
            log_entry("scale_image_leptonica_cuda: finished image scaling",
                      image_state);
        }
    }
#endif // __WIN32__

    return scaled;
}

