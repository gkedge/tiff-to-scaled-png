package com.kedges.tiffscaledpng;

/**
 * gkedge - 12/30/13.
 */
public class TiffToScaledPngJNI {

    /**
     * Convert tiff to scaled png jni.
     *
     * @param imageState
     *         the image state
     * @return the int
     */
    private static native int convertTiffToScaledPngJNI(State imageState);

    /**
     * Function to call native conversion code.
     * <p/>
     * All the interesting stuff is in the imageState that holds all the
     * operation's input and output reflective updated by the native code...
     *
     * @param imageState
     *         the handle that holds a single conversion operations input.
     *         This instance is reflectively updated by the native code. Upon
     *         return, it will contain the converted PNG and optionally
     *         gathered telemetry.
     * @see State
     */
    public static void convertTiffToScaledPng(State imageState) {
        try {
            convertTiffToScaledPngJNI(imageState);
        } catch (TiffToScaledPngException ttspe) {
            throw new TiffToScaledPngException("Failed to convert TIFF to scaled PNG.", ttspe);
        }
        /*
         * catch (Exception e) { throw new
         * TiffToScaledPngException("Failed to convert TIFF to scaled PNG.", e);
         * }
         */
    }

}
