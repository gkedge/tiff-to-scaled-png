package com.kedges.tiffscaledpng;

import com.jezhumble.javasysmon.JavaSysMon;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * gkedge - 4/4/2014.
 */
@edu.umd.cs.findbugs.annotations.SuppressWarnings(
        value = "STCAL_STATIC_SIMPLE_DATE_FORMAT_INSTANCE",
        justification = "Reference to date formatter is synchronized.")
public class ImageConversionDriver {

    static {
        System.out.printf("Usr dir: %s%n", SystemUtils.getUserDir());
    }

    /** The Constant reportWriteLock. */
    private static final ReentrantReadWriteLock.WriteLock REPORT_WRITE_LOCK =
            new ReentrantReadWriteLock().writeLock();

    /** The Constant REPORT_DATE_TIME_FORMAT. */
    private static final DateFormat REPORT_DATE_TIME_FORMAT = new SimpleDateFormat("MMdd:HH:mm:ss:SSS");

    /** The Constant pathToFileContentMap. */
    private static final Map<String, byte[]> PATH_TO_FILE_CONTENT_MAP = new HashMap<String, byte[]>();

    /** The Constant REPORT_HEADER. */
    private static final String REPORT_HEADER = "Time(MMdd:HH:mm:ss:SSS),CPU(GHz),Host,Conversion Path,"
            + "Src Width(px),Src Height(px),Src Size(px),"
            + "Dest Width(px),Dest Height(px),Dest Size(px),"
            + "TIFF Decompress(ms),"
            + "TIFF Decompress StdDev(ms),"
            + "Scale(ms),"
            + "Scale StdDev(ms),"
            + "GPU,"
            + "GPU Wait (ms),"
            + "GPU Wait StdDev(ms),"
            + "PNG encode(ms),"
            + "PNG encode StdDev(ms),"
            + "Total Image Conversion(ms),"
            + "Total Image Conversion StdDev(ms)\n";

    /** The Constant testImageDirectory. */
    private static final File TEST_IMAGE_DIRECTORY =
            new File(System.getProperty("java.io.tmpdir") + File.separator + "ImageConversions");

    /** The Constant MAX_THREADS. */
    private static final int MAX_THREADS = Runtime.getRuntime().availableProcessors();

    /** The Constant cpuFrequency. */
    private static final double CPU_FREQUENCY_TO_HZ = 1000000000.0;

    /** The Constant CPU_FREQUENCY. */
    private static final double CPU_FREQUENCY = new JavaSysMon().cpuFrequencyInHz() / CPU_FREQUENCY_TO_HZ;

    /** The Constant executor. */
    private static final ThreadPoolExecutor EXECUTOR =
            new ThreadPoolExecutor(MAX_THREADS, MAX_THREADS,
                    0L, TimeUnit.MILLISECONDS,
                    new ArrayBlockingQueue<Runnable>(4, true),
                    new DaemonThreadFactory());

    static {
        Logger.getLogger(State.class).setLevel(Level.DEBUG);
        State.setNativeDebugLevel();
        RejectedExecutionHandler block = new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                try {
                    executor.getQueue().put(r);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        EXECUTOR.setRejectedExecutionHandler(block);
        System.out.printf("Sys: %s; GPU: %s; CPU Speed: %.02fGHz; Threads: %d%n",
                SystemUtils.OS_NAME, Boolean.toString(State.isHasGpu()).toUpperCase(),
                CPU_FREQUENCY, MAX_THREADS);
    }

    /** The Constant CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP. */
    private static final Map<Conversion, BlockingQueue<ConversionReport>> CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP =
            Collections.synchronizedMap(new HashMap<Conversion, BlockingQueue<ConversionReport>>());

    /** The Constant WRITE_REPORT_QUEUE_MAP. */
    private static final Map<State.ImageChain, BlockingQueue<ConversionReport>> WRITE_REPORT_QUEUE_MAP = Collections
            .synchronizedMap(new HashMap<State.ImageChain, BlockingQueue<ConversionReport>>());

    /** The Constant TIF_FILE_EXTENSION. */
    private static final String TIF_FILE_EXTENSION = ".tif";

    /**
     * The main method.
     *
     * @param args
     *         the arguments
     */
    public static void main(String[] args) {
        final String commandLineMessage1 = "Waiting for [Enter]";
        try {
            copyTestImages();
            File[] fileList = TEST_IMAGE_DIRECTORY.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(TIF_FILE_EXTENSION);
                }
            });

            System.out.println("Cleaning temp area...");
            readAllTiffImagesIntoMemory(fileList);
            System.out.println("Temp area cleaned...");

            Scanner sc = new Scanner(System.in);
            System.out.println(commandLineMessage1);
            sc.hasNextLine();
            System.out.println("Off to the races...");
            for (State.ImageChain imageChain : new State.ImageChain[]{State.ImageChain.LEPTONICA}) {
                WRITE_REPORT_QUEUE_MAP.put(imageChain, new LinkedBlockingDeque<ConversionReport>());
                String pathname = imageChain.toString().toLowerCase() + "_report_tiff_to_scaled_png.csv";
                startWriteReportThread(imageChain, initReport(new File(pathname)));

                for (int scaledWidth = 100, jj = 1; scaledWidth < (2550 * 5); scaledWidth = scaledWidth < 2550 ? scaledWidth + 10
                        : scaledWidth + 50) {
                    System.out.printf("Start\t%s [%dpx]\n", imageChain.toString().toLowerCase(), scaledWidth);
                    Conversion conversion = new Conversion(imageChain, scaledWidth);
                    CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP.put(conversion, new LinkedBlockingDeque<ConversionReport>());
                    startConversionToPixelReportThread(conversion, fileList.length);

                    for (File tiffFile : fileList) {
                        String fullTiffPath = tiffFile.getCanonicalPath();
                        String fullTiffPathSuffixRemoved =
                                fullTiffPath.substring(0, fullTiffPath.lastIndexOf(TIF_FILE_EXTENSION));
                        String baseFileName =
                                fullTiffPathSuffixRemoved.substring(fullTiffPathSuffixRemoved.lastIndexOf(File.separator) + 1);
                        String pxDirectoryName =
                                String.format("%s%s%d", fullTiffPathSuffixRemoved, File.separator, scaledWidth);
                        String compressedFileName = null;
                        if (conversion.isSavePng()) {
                            File pxDirectory = new File(pxDirectoryName);
                            FileUtils.forceMkdir(pxDirectory);
                            compressedFileName = pxDirectoryName + File.separator + baseFileName
                                    + imageChain.getSuffix() + ".png";
                        }
                        convertTiffToPng(conversion, fullTiffPath, compressedFileName);
                    }
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        System.out.println("\nFinished submitting image requests.\n");
        EXECUTOR.shutdown();

        Scanner sc = new Scanner(System.in);
        System.out.println(commandLineMessage1);
        sc.hasNextLine();
    }

    /**
     * Start conversion to pixel report thread.
     *
     * @param conversion
     *         the conversion
     * @param sampleCount
     *         the sample count
     */
    private static void startConversionToPixelReportThread(final Conversion conversion, final int sampleCount) {
        final BlockingQueue<ConversionReport> conversionReportQueue =
                CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP.get(conversion);
        final Queue<ConversionReport> conversionReports =
                new LinkedList<ConversionReport>();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                try {
                    while (conversionReports.size() < sampleCount) {
                        conversionReports.add(conversionReportQueue.take());
                        conversionReportQueue.drainTo(conversionReports);
                    }
                    double sampleCountD = sampleCount;
                    double imageDecompressTime = 0.0;
                    double imageScaleTime = 0.0;
                    double imageEncodePngTime = 0.0;
                    double imageConvertTime = 0.0;
                    double gpuWaitTime = 0.0;
                    ConversionReport lastReport = null;
                    for (ConversionReport convertReport : conversionReports) {
                        lastReport = convertReport;
                        imageDecompressTime += convertReport.getImageDecompressTime();
                        imageScaleTime += convertReport.getImageScaleTime();
                        imageEncodePngTime += convertReport.getImageEncodePngTime();
                        imageConvertTime += convertReport.getImageConvertTime();
                        gpuWaitTime += convertReport.getGpuWaitTime();
                        // System.out.print(convertReport.getGpuDevice());
                    }
                    if (lastReport != null) {
                        imageDecompressTime /= sampleCountD;
                        imageScaleTime /= sampleCountD;
                        imageEncodePngTime /= sampleCountD;
                        imageConvertTime /= sampleCountD;
                        gpuWaitTime /= sampleCountD;

                        double imageDecompressTimeSum = 0.0f;
                        double imageScaleTimeSum = 0.0f;
                        double imageEncodePngTimeSum = 0.0f;
                        double imageConvertTimeSum = 0.0f;
                        double gpuWaitTimeSum = 0.0f;
                        for (ConversionReport convertReport : conversionReports) {
                            double data = convertReport.getImageDecompressTime();
                            imageDecompressTimeSum += (data - imageDecompressTime) * (data - imageDecompressTime);
                            data = convertReport.getImageScaleTime();
                            imageScaleTimeSum += (data - imageScaleTime) * (data - imageScaleTime);
                            data = convertReport.getImageEncodePngTime();
                            imageEncodePngTimeSum += (data - imageEncodePngTime) * (data - imageEncodePngTime);
                            data = convertReport.getImageConvertTime();
                            imageConvertTimeSum += (data - imageConvertTime) * (data - imageConvertTime);
                            data = convertReport.getGpuWaitTime();
                            gpuWaitTimeSum += (data - gpuWaitTime) * (data - gpuWaitTime);
                        }

                        double imageDecompressTimeStdDev =
                                Math.sqrt(imageDecompressTimeSum / sampleCountD);

                        double imageScaleTimeStdDev =
                                Math.sqrt(imageScaleTimeSum / sampleCountD);

                        double imageEncodePngTimeStdDev =
                                Math.sqrt(imageEncodePngTimeSum / sampleCountD);

                        double imageConvertTimeStdDev =
                                Math.sqrt(imageConvertTimeSum / sampleCountD);

                        double gpuWaitTimeStdDev =
                                Math.sqrt(gpuWaitTimeSum / sampleCountD);

                        BlockingQueue<ConversionReport> writeReportQueue =
                                WRITE_REPORT_QUEUE_MAP.get(conversion.getImageChain());

                        ConversionReport conversionReport =
                                new ConversionReport(conversion, lastReport.getTiffDim(),
                                        lastReport.getPngDim(),
                                        imageDecompressTime, imageDecompressTimeStdDev,
                                        imageScaleTime, imageScaleTimeStdDev, lastReport.getGpuDevice(),
                                        imageEncodePngTime, imageEncodePngTimeStdDev,
                                        imageConvertTime, imageConvertTimeStdDev,
                                        gpuWaitTime, gpuWaitTimeStdDev);
                        writeReportQueue.put(conversionReport);
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                } finally {
                    CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP.remove(conversion);
                }
            }
        };

        Thread reportThread = new Thread(runnable,
                conversion.getImageChain().toString().toLowerCase() + "-" + conversion.getScaledWidth());
        reportThread.start();
    }

    /**
     * Start write report thread.
     *
     * @param imageChain
     *         the image chain
     * @param imageChainReport
     *         the image chain report
     * @throws InterruptedException
     *         the interrupted exception
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static void startWriteReportThread(final State.ImageChain imageChain, final Writer imageChainReport)
            throws InterruptedException, IOException {
        final BlockingQueue<ConversionReport> conversionReportQueue = WRITE_REPORT_QUEUE_MAP.get(imageChain);
        final Queue<ConversionReport> conversionReports = new LinkedList<ConversionReport>();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    for (; ; ) { // forever until break out...
                        if (EXECUTOR.isTerminated() && CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP.isEmpty()) {
                            ConversionReport report = conversionReportQueue.peek();
                            if (report == null) {
                                break;
                            }
                            conversionReports.add(report);
                        } else {
                            ConversionReport report = conversionReportQueue.poll(5, TimeUnit.SECONDS);
                            if (report == null) {
                                continue;
                            }
                            conversionReports.add(report);
                        }
                        conversionReportQueue.drainTo(conversionReports);

                        while (!conversionReports.isEmpty()) {
                            ConversionReport convertReport = conversionReports.remove();
                            Conversion conversion = convertReport.getConversion();
                            System.out.printf("Updating %s [%d]%n",
                                    conversion.getImageChain().toString().toLowerCase(), conversion.getScaledWidth());
                            updateImageChainReport(imageChainReport,
                                    SystemUtils.OS_NAME,
                                    conversion.getImageChain(),
                                    convertReport);
                        }
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                } finally {
                    try {
                        System.out.printf("Closing %s\n", imageChain.toString());
                        imageChainReport.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread reportThread = new Thread(runnable, imageChain.name().toLowerCase() + "_thread");
        reportThread.start();
    }

    /** Read all tiff images into memory. */
    private static final String RANDOM_ACCESS_MODE = "r";

    /**
     * Read all tiff images into memory.
     *
     * @param list
     *         the list
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static void readAllTiffImagesIntoMemory(File[] list) throws IOException {
        for (File tiffFile : list) {
            String fullTiffPath = tiffFile.getCanonicalPath();
            String fullTiffPathSuffixRemoved =
                    fullTiffPath.substring(0, fullTiffPath.lastIndexOf(TIF_FILE_EXTENSION));
            File pngDirectory = new File(fullTiffPathSuffixRemoved);
            FileUtils.deleteDirectory(pngDirectory);

            RandomAccessFile tiffTestFile = new RandomAccessFile(fullTiffPath, RANDOM_ACCESS_MODE);
            // Get and check length
            long longlength = tiffTestFile.length();
            int length = (int) longlength;
            if (length != longlength) {
                tiffTestFile.close();
                throw new IOException(IO_EXCEPTION_ERROR_MESSAGE);
            }
            // Read TIFF into buffer set in State.
            byte[] data = new byte[length];
            tiffTestFile.readFully(data);
            tiffTestFile.close();
            PATH_TO_FILE_CONTENT_MAP.put(fullTiffPath, data);
        }
    }

    /**
     * Copy test images.
     *
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static void copyTestImages() throws IOException {
        File testImageDirectorySrc = new File("tiff-to-scaled-png-jni/src/test/resources/testImages/PTO");
        FileUtils.forceMkdir(TEST_IMAGE_DIRECTORY);
        FileUtils.copyDirectory(testImageDirectorySrc, TEST_IMAGE_DIRECTORY);

        String[] list = TEST_IMAGE_DIRECTORY.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(TIF_FILE_EXTENSION);
            }
        });

        for (String tiffFileName : list) {
            String subdirectory = tiffFileName.substring(0, tiffFileName.lastIndexOf(TIF_FILE_EXTENSION));
            String pathname = String.format("%s%s%s", TEST_IMAGE_DIRECTORY, File.separator, subdirectory);
            File directory = new File(pathname);
            if (!directory.exists()) {
                FileUtils.forceMkdir(directory);
            }
        }
    }

    /** Convert tiff to png. */
    private static final String IO_EXCEPTION_ERROR_MESSAGE = "File size >= 2 GB";

    /**
     * Convert tiff to png.
     *
     * @param conversion
     *         the conversion
     * @param tiffFileName
     *         the tiff file name
     * @param compressedFileName
     *         the compressed file name
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static void convertTiffToPng(final Conversion conversion, final String tiffFileName,
                                         final String compressedFileName) throws IOException {
        byte[] data = PATH_TO_FILE_CONTENT_MAP.get(tiffFileName);

        if (data == null) {
            RandomAccessFile tiffTestFile = new RandomAccessFile(tiffFileName, RANDOM_ACCESS_MODE);
            // Get and check length
            long longlength = tiffTestFile.length();
            int length = (int) longlength;
            if (length != longlength) {
                tiffTestFile.close();
                throw new IOException(IO_EXCEPTION_ERROR_MESSAGE);
            }
            // Read TIFF into buffer set in State.
            data = new byte[length];
            tiffTestFile.readFully(data);
            tiffTestFile.close();
            PATH_TO_FILE_CONTENT_MAP.put(tiffFileName, data);
        }

        final byte[] finalData = data;
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                RandomAccessFile compressedFile = null;
                try {
                    State state =
                            new State(conversion.getImageChain(), tiffFileName,
                                    finalData, conversion.getScaledWidth());

                    TiffToScaledPngJNI.convertTiffToScaledPng(state);
                    BlockingQueue<ConversionReport> conversionToPixelReport =
                            CONVERSION_TO_PIXEL_REPORT_QUEUE_MAP.get(conversion);
                    ConversionReport conversionReport =
                            new ConversionReport(conversion,
                                    new Dimension(state.getTiffWidth(), state.getTiffHeight()),
                                    new Dimension(state.getPngWidth(), state.getPngHeight()),
                                    state.getImageDecompressTime(), 0.0f,
                                    state.getImageScaleTime(), 0.0f, state.getGpuDevice(),
                                    state.getImageEncodePngTime(), 0.0f,
                                    state.getImageConvertTime(), 0.0f,
                                    state.getGpuWaitTime(), state.getGpuWaitTime());
                    conversionToPixelReport.add(conversionReport);
                    if (conversion.isSavePng()) {
                        compressedFile = new RandomAccessFile(compressedFileName, "rw");
                        compressedFile.write(state.getPngImage());
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                } finally {
                    if (compressedFile != null) {
                        try {
                            compressedFile.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        EXECUTOR.submit(runnable);
    }

    /**
     * Update image chain report.
     *
     * @param report
     *         the report
     * @param host
     *         the host
     * @param imageChain
     *         the image chain
     * @param convertReport
     *         the convert report
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static void updateImageChainReport(Writer report, String host, State.ImageChain imageChain,
                                               ConversionReport convertReport) throws IOException {
        try {

            long tiffImageSize = convertReport.getTiffDim().width * convertReport.getTiffDim().height;
            long pngImageSize = convertReport.getPngDim().width * convertReport.getPngDim().height;
            REPORT_WRITE_LOCK.lock();
            report.append(String.format(
                    "%s,%s,%.02f,%s,%d,%d,%d,%d,%d,%d,%.3f,%.3f,%.3f,%.3f,%d,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f%n",
                    REPORT_DATE_TIME_FORMAT.format(new Date()),
                    host,
                    CPU_FREQUENCY,
                    imageChain.toString().toLowerCase(),
                    convertReport.getTiffDim().width,
                    convertReport.getTiffDim().height,
                    tiffImageSize,
                    convertReport.getPngDim().width,
                    convertReport.getPngDim().height,
                    pngImageSize,
                    convertReport.getImageDecompressTime(),
                    convertReport.getImageDecompressTimeStdDev(),
                    convertReport.getImageScaleTime(),
                    convertReport.getImageScaleTimeStdDev(),
                    convertReport.getGpuDevice(),
                    convertReport.getGpuWaitTime(),
                    convertReport.getGpuWaitTimeStdDev(),
                    convertReport.getImageEncodePngTime(),
                    convertReport.getImageCompressTimeStdDev(),
                    convertReport.getImageConvertTime(),
                    convertReport.getImageConvertTimeStdDev()
            ));
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to update report.", e);
        } finally {
            REPORT_WRITE_LOCK.unlock();
        }
    }

    /**
     * Inits the report.
     *
     * @param reportFile
     *         the report file
     * @return the writer
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static Writer initReport(File reportFile) throws IOException {
        Writer report;
        try {
            boolean append = reportFile.exists();
            report = new BufferedWriter(new FileWriter(reportFile, append), 64 * 1024);
            if (!append) {
                report.append(REPORT_HEADER);
            }

        } catch (IOException e) {
            throw new TiffToScaledPngException("Failed to create report file: " + reportFile.getAbsolutePath(), e);
        }

        return report;
    }

    /**
     * The Class Conversion.
     */
    private static class Conversion {

        /** The image chain. */
        private final State.ImageChain imageChain;

        /** The scaled width. */
        private final int scaledWidth;

        /** The save png. */
        private boolean savePng = false;

        /**
         * Instantiates a new conversion.
         *
         * @param imageChain
         *         the image chain
         * @param scaledWidth
         *         the scaled width
         */
        private Conversion(State.ImageChain imageChain, int scaledWidth) {
            this.imageChain = imageChain;
            this.scaledWidth = scaledWidth;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Conversion that = (Conversion) o;

            return imageChain == that.imageChain && scaledWidth == that.scaledWidth;
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            int result = imageChain.getJniConstant();
            result = 31 * result + scaledWidth;
            return result;
        }

        /**
         * Gets the image chain.
         *
         * @return the image chain
         */
        public State.ImageChain getImageChain() {
            return imageChain;
        }

        /**
         * Gets the scaled width.
         *
         * @return the scaled width
         */
        public int getScaledWidth() {
            return scaledWidth;
        }

        /**
         * Checks if is save png.
         *
         * @return true, if is save png
         */
        public boolean isSavePng() {
            return savePng;
        }

        /**
         * Sets the save png.
         *
         * @param savePng
         *         the new save png
         */
        public void setSavePng(boolean savePng) {
            this.savePng = savePng;
        }
    }

    /**
     * The Class ConversionReport.
     */
    private static final class ConversionReport {

        /** The conversion. */
        private final Conversion conversion;

        /** The tiff dim. */
        private final Dimension tiffDim;
        /** The png dim. */
        private final Dimension pngDim;
        /** The image decompress time. */
        private final double imageDecompressTime;

        /** The image decompress time std dev. */
        private final double imageDecompressTimeStdDev;

        /** The image scale time. */
        private final double imageScaleTime;

        /** The image scale time std dev. */
        private final double imageScaleTimeStdDev;

        /** The gpu device. */
        private final int gpuDevice;

        /** The image encode png time. */
        private final double imageEncodePngTime;

        /** The image compress time std dev. */
        private final double imageCompressTimeStdDev;

        /** The image convert time. */
        private final double imageConvertTime;

        /** The image convert time std dev. */
        private final double imageConvertTimeStdDev;

        /** The gpu wait time. */
        private final double gpuWaitTime;

        /** The gpu wait time std dev. */
        private final double gpuWaitTimeStdDev;

        /**
         * Instantiates a new conversion report.
         *
         * @param conversion
         *         the conversion
         * @param tiffDim
         *         the tiff dim
         * @param pngDim
         *         the png dim
         * @param imageDecompressTime
         *         the image decompress time
         * @param imageDecompressTimeStdDev
         *         the image decompress time std dev
         * @param imageScaleTime
         *         the image scale time
         * @param imageScaleTimeStdDev
         *         the image scale time std dev
         * @param gpuDevice
         *         the gpu device
         * @param imageEncodePngTime
         *         the image encode png time
         * @param imageCompressTimeStdDev
         *         the image compress time std dev
         * @param imageConvertTime
         *         the image convert time
         * @param imageConvertTimeStdDev
         *         the image convert time std dev
         * @param gpuWaitTime
         *         the gpu wait time
         * @param gpuWaitTimeStdDev
         *         the gpu wait time std dev
         */
        private ConversionReport(Conversion conversion, Dimension tiffDim, Dimension pngDim,
                                 double imageDecompressTime, double imageDecompressTimeStdDev,
                                 double imageScaleTime, double imageScaleTimeStdDev, int gpuDevice,
                                 double imageEncodePngTime, double imageCompressTimeStdDev,
                                 double imageConvertTime, double imageConvertTimeStdDev,
                                 double gpuWaitTime, double gpuWaitTimeStdDev) {
            this.conversion = conversion;
            this.tiffDim = tiffDim;
            this.pngDim = pngDim;
            this.imageDecompressTime = imageDecompressTime;
            this.imageDecompressTimeStdDev = imageDecompressTimeStdDev;
            this.imageScaleTime = imageScaleTime;
            this.imageScaleTimeStdDev = imageScaleTimeStdDev;
            this.gpuDevice = gpuDevice;
            this.imageEncodePngTime = imageEncodePngTime;
            this.imageCompressTimeStdDev = imageCompressTimeStdDev;
            this.imageConvertTime = imageConvertTime;
            this.imageConvertTimeStdDev = imageConvertTimeStdDev;
            this.gpuWaitTime = gpuWaitTime;
            this.gpuWaitTimeStdDev = gpuWaitTimeStdDev;
        }

        /**
         * Gets the tiff dim.
         *
         * @return the tiff dim
         */
        public Dimension getTiffDim() {
            return tiffDim;
        }

        /**
         * Gets the png dim.
         *
         * @return the png dim
         */
        public Dimension getPngDim() {
            return pngDim;
        }

        /**
         * Gets the image decompress time.
         *
         * @return the image decompress time
         */
        public double getImageDecompressTime() {
            return imageDecompressTime;
        }

        /**
         * Gets the image decompress time std dev.
         *
         * @return the image decompress time std dev
         */
        public double getImageDecompressTimeStdDev() {
            return imageDecompressTimeStdDev;
        }

        /**
         * Gets the image scale time.
         *
         * @return the image scale time
         */
        public double getImageScaleTime() {
            return imageScaleTime;
        }

        /**
         * Gets the image scale time std dev.
         *
         * @return the image scale time std dev
         */
        public double getImageScaleTimeStdDev() {
            return imageScaleTimeStdDev;
        }

        /**
         * Gets the gpu device.
         *
         * @return the gpu device
         */
        public int getGpuDevice() {
            return gpuDevice;
        }

        /**
         * Gets the image encode png time.
         *
         * @return the image encode png time
         */
        public double getImageEncodePngTime() {
            return imageEncodePngTime;
        }

        /**
         * Gets the image compress time std dev.
         *
         * @return the image compress time std dev
         */
        public double getImageCompressTimeStdDev() {
            return imageCompressTimeStdDev;
        }

        /**
         * Gets the image convert time.
         *
         * @return the image convert time
         */
        public double getImageConvertTime() {
            return imageConvertTime;
        }

        /**
         * Gets the image convert time std dev.
         *
         * @return the image convert time std dev
         */
        public double getImageConvertTimeStdDev() {
            return imageConvertTimeStdDev;
        }

        /**
         * Gets the conversion.
         *
         * @return the conversion
         */
        public Conversion getConversion() {
            return conversion;
        }

        /**
         * Gets the gpu wait time.
         *
         * @return the gpu wait time
         */
        public double getGpuWaitTime() {
            return gpuWaitTime;
        }

        /**
         * Gets the gpu wait time std dev.
         *
         * @return the gpu wait time std dev
         */
        public double getGpuWaitTimeStdDev() {
            return gpuWaitTimeStdDev;
        }
    }

    /**
     * A factory for creating DaemonThread objects.
     */
    private static class DaemonThreadFactory implements ThreadFactory {

        /** The Constant poolNumber. */
        static final AtomicInteger POOL_NUMBER = new AtomicInteger(1);

        /** The group. */
        private final ThreadGroup group;

        /** The thread number. */
        private final AtomicInteger threadNumber = new AtomicInteger(1);

        /** The name prefix. */
        private final String namePrefix;

        /**
         * Instantiates a new daemon thread factory.
         */
        DaemonThreadFactory() {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup()
                    : Thread.currentThread().getThreadGroup();
            namePrefix = "pool-"
                    + POOL_NUMBER.getAndIncrement()
                    + "-daemon-thread-";
        }

        /*
         * (non-Javadoc)
         *
         * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
         */
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(group, r,
                    namePrefix + threadNumber.getAndIncrement(),
                    0);
            if (!t.isDaemon()) {
                t.setDaemon(true);
            }
            if (t.getPriority() != Thread.NORM_PRIORITY) {
                t.setPriority(Thread.NORM_PRIORITY);
            }
            return t;
        }
    }

}
