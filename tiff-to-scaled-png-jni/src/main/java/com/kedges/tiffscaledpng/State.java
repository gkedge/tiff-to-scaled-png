package com.kedges.tiffscaledpng;

import com.jezhumble.javasysmon.CpuTimes;
import com.jezhumble.javasysmon.JavaSysMon;
import org.apache.log4j.*;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * All State fields (class or instance) are accessed (either read or set)
 * reflectively in the native code. So, if your IDE is telling you that some
 * field is never used, check the native code before cleaning it up!
 */
@edu.umd.cs.findbugs.annotations.SuppressWarnings(
        value = "STCAL_STATIC_SIMPLE_DATE_FORMAT_INSTANCE",
        justification = "Reference to date formatter is synchronized.")
public class State {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(State.class);

    /**
     * CONVERTER_LEVEL_LOG controls the debug logging level within the native
     * code. It's level value is based on State's Log4j level. So, if root Log4j
     * is set to Level.DEBUG, but you are not interested in impacting the native
     * execution with debug overhead, add a g.u.p.image.State-specific
     * configuration setting in the Log4j config file to shunt/control native
     * logging independent of the root level.
     * <p/>
     * One good reason not to debug log in native code: like Log4j, each log
     * entry is flushed to disk immediately! IOW: slow!
     */
    private static int CONVERTER_LEVEL_LOG = Integer.MIN_VALUE; // Read
    // reflectively
    // in native
    // code!

    /** The png compression level. */
    private static int PNG_COMPRESSION_LEVEL = 1; // Read reflectively in native
    // code!

    /** The Constant LOG_LEVEL_NONE. */
    private static final int LOG_LEVEL_NONE = 0;
    /* Gather telemetry for image chain timing report. */
    /** The Constant LOG_LEVEL_TELEMETRY. */
    private static final int LOG_LEVEL_TELEMETRY = 1;

    /** The Constant LOG_LEVEL_DEBUG. */
    private static final int LOG_LEVEL_DEBUG = 2;

    /** The Constant LOG_LEVEL_TRACE. */
    private static final int LOG_LEVEL_TRACE = 3;

    /** The Constant OPENCV_JNI_CONSTANT. */
    private static final int OPENCV_JNI_CONSTANT = 0;

    /** The Constant OPENCV_CUDA_JNI_CONSTANT. */
    private static final int OPENCV_CUDA_JNI_CONSTANT = 1;

    /** The Constant LEPTONICA_JNI_CONSTANT. */
    private static final int LEPTONICA_JNI_CONSTANT = 2;

    /** The Constant LEPTONICA_CUDA_JNI_CONSTANT. */
    private static final int LEPTONICA_CUDA_JNI_CONSTANT = 3;

    /** The Constant LEPTONICA_HYBRID_JNI_CONSTANT. */
    private static final int LEPTONICA_HYBRID_JNI_CONSTANT = 4;

    /** The Constant NATIVE_LOG_FILE. */
    private static final String NATIVE_LOG_FILE = "tiff_to_scaled_png.log";

    /**
     * The Enum ImageChain.
     */
    public enum ImageChain {

        /** The opencv. */
        OPENCV(OPENCV_JNI_CONSTANT, "CV"),

        /** The opencv cuda. */
        OPENCV_CUDA(OPENCV_CUDA_JNI_CONSTANT, "CV_GPU"),

        /** The leptonica. */
        LEPTONICA(LEPTONICA_JNI_CONSTANT, "L"),

        /** The leptonica cuda. */
        LEPTONICA_CUDA(LEPTONICA_CUDA_JNI_CONSTANT, "L_GPU"),

        /** The leptonica hybrid. */
        LEPTONICA_HYBRID(LEPTONICA_HYBRID_JNI_CONSTANT, "L_HYD");

        /** The jni constant. */
        private final int jniConstant;

        /** The suffix. */
        private final String suffix;

        /**
         * Instantiates a new image chain.
         *
         * @param jniConstant
         *         the jni constant
         * @param suffix
         *         the suffix
         */
        ImageChain(int jniConstant, String suffix) {
            this.jniConstant = jniConstant;
            this.suffix = suffix;
        }

        /**
         * Gets the suffix.
         *
         * @return the suffix
         */
        public String getSuffix() {
            return suffix;
        }

        /**
         * Gets the jni constant.
         *
         * @return the jni constant
         */
        public int getJniConstant() {
            return jniConstant;
        }

        /**
         * Gets the image chain.
         *
         * @param jniConstant
         *         the jni constant
         * @return the image chain
         */
        public static ImageChain getImageChain(int jniConstant) {
            switch (jniConstant) {
                case OPENCV_JNI_CONSTANT:
                    return OPENCV;
                case OPENCV_CUDA_JNI_CONSTANT:
                    return OPENCV_CUDA;
                case LEPTONICA_JNI_CONSTANT:
                    return LEPTONICA;
                case LEPTONICA_CUDA_JNI_CONSTANT:
                    return LEPTONICA_CUDA;
                case LEPTONICA_HYBRID_JNI_CONSTANT:
                    return LEPTONICA_HYBRID;
                default:
                    break;
            }
            return OPENCV;
        }
    }

    static {
        setNativeDebugLevel();
    }

    /** The Constant LOGGING_ROOT_PATH. */
    private static final String LOGGING_ROOT_PATH;

    /** The Constant USER_DIR. */
    private static final String USER_DIR = "user.dir";

    static {
        if (CONVERTER_LEVEL_LOG >= LOG_LEVEL_TELEMETRY) {
            String logPath = System.getProperty(USER_DIR);

            SortedMap<String, Logger> sortedLoggers = new TreeMap<String, Logger>();
            Logger rootLogger = Logger.getRootLogger();
            if (rootLogger != null) {
                sortedLoggers.put("", rootLogger);
            }
            Enumeration<Logger> allLoggers = LogManager.getCurrentLoggers();
            while (allLoggers.hasMoreElements()) {
                Logger logger = allLoggers.nextElement();
                String loggerName = logger.getName();
                if (loggerName.isEmpty() || State.class.getName().contains(loggerName)) {
                    sortedLoggers.put(loggerName, logger);
                }
            }

            for (Map.Entry<String, Logger> loggerEntry : sortedLoggers.entrySet()) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Logger name: " + loggerEntry.getKey());
                }
                Logger log = loggerEntry.getValue();
                Enumeration<Appender> allAppenders = log.getAllAppenders();
                while (allAppenders.hasMoreElements()) {
                    Appender appender = allAppenders.nextElement();
                    LOGGER.debug("Found appender: " + appender.getName());
                    if (appender instanceof FileAppender) {
                        FileAppender fileAppender = (FileAppender) appender;
                        String fileAppenderFilename = fileAppender.getFile();
                        File fileAppenderFile = new File(fileAppenderFilename);
                        String parent = fileAppenderFile.getParent();
                        logPath = parent != null ? parent : logPath;
                        break;
                    }
                }
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found file appender path: "
                        + !logPath.equals(System.getProperty(USER_DIR)));
            }
            LOGGING_ROOT_PATH = logPath;
        } else {
            LOGGING_ROOT_PATH = "";
        }
    }

    /** The Constant NATIVE_LOG_FILENAME. */
    private static final String NATIVE_LOG_FILENAME =
            System.getProperty("NativeLogFileName", NATIVE_LOG_FILE);
    /**
     * Native logging will NOT attempt to tap directly into Log4j; the overhead
     * of doing so is prohibitive. However, the native log file will be placed
     * into the same directory as used Log4j's file appender. If no file
     * appender is in use, USER_DIR will server as the directory containing the
     * native log file.
     */
    private static final String NATIVE_LOG_PATH; // Read reflectively in native
    // code!

    static {
        if (CONVERTER_LEVEL_LOG >= LOG_LEVEL_DEBUG) {
            String logPath =
                    LOGGING_ROOT_PATH.endsWith(File.separator) ? LOGGING_ROOT_PATH : LOGGING_ROOT_PATH + File.separator;
            NATIVE_LOG_PATH = logPath + NATIVE_LOG_FILENAME;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Native log file: " + NATIVE_LOG_PATH);
            }
        } else {
            NATIVE_LOG_PATH = "";
        }
    }

    /** The Constant REPORT_HEADER. */
    public static final String REPORT_HEADER = "Time(MMdd:HH:mm:ss:SSS),Host,Path,Conversion Path,"
            + "Src Width(px),Src Height(px),Src Size(px),Src Compressed Size(bytes),"
            + "Dest Width(px),Dest Height(px),Dest Size(px),Dest Compressed Size(bytes),"
            + "Size Change(%),"
            + "TIFF Decompress(ms),"
            + "Scale(ms),"
            + "GPU,"
            + "PNG encode(ms),"
            + "Total Image Conversion(ms),"
            + "Native Overhead(ms),"
            + "Total Request(ms),"
            + "Java Latency (ms)\n";

    /** The Constant NATIVE_SHARED_LIB_NAME. */
    public static final String NATIVE_SHARED_LIB_NAME = "tiffToScaledPng";

    /** The Constant REPORT_CSV_FILENAME. */
    private static final String REPORT_CSV_FILENAME = "report_tiff_to_scaled_png.csv";

    /** The Constant REPORT_CSV_PATH. */
    private static final String REPORT_CSV_PATH;

    static {
        if (CONVERTER_LEVEL_LOG >= LOG_LEVEL_TELEMETRY) {
            String logPath =
                    LOGGING_ROOT_PATH.endsWith(File.separator) ? LOGGING_ROOT_PATH : LOGGING_ROOT_PATH + File.separator;
            REPORT_CSV_PATH = logPath + REPORT_CSV_FILENAME;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Native telemetry file: " + REPORT_CSV_PATH);
            }
        } else {
            REPORT_CSV_PATH = "";
        }
    }

    /** The Constant reportWriteLock. */
    private static final ReentrantReadWriteLock.WriteLock REPORT_WRITE_LOCK =
            new ReentrantReadWriteLock().writeLock();

    /** The report. */
    private static Writer report;

    /** The Constant REPORT_DATE_TIME_FORMAT. */
    public static final DateFormat REPORT_DATE_TIME_FORMAT = new SimpleDateFormat("MMdd:HH:mm:ss:SSS");

    /** The Constant CPU_UTIL_FILENAME. */
    public static final String CPU_UTIL_FILENAME = "cpu_utilization.bin";

    /** The Constant CPU_UTIL_PATH. */
    private static final String CPU_UTIL_PATH;

    /** The Constant CPU_UTIL_REPORT_EXTENSION. */
    private static final String CPU_UTIL_REPORT_EXTENSION = ".CpuUtilReport";

    static {
        boolean measureCpuUtilReport = CONVERTER_LEVEL_LOG >= LOG_LEVEL_TELEMETRY;
        measureCpuUtilReport = measureCpuUtilReport
                && Boolean.getBoolean(State.class.getName() + CPU_UTIL_REPORT_EXTENSION);
        if (measureCpuUtilReport) {
            String logPath =
                    LOGGING_ROOT_PATH.endsWith(File.separator) ? LOGGING_ROOT_PATH :
                            LOGGING_ROOT_PATH + File.separator;
            CPU_UTIL_PATH = logPath + CPU_UTIL_FILENAME;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Native telemetry file: " + CPU_UTIL_PATH);
            }
        } else {
            CPU_UTIL_PATH = "";
        }
    }

    /** The cpu utilization. */
    private static DataOutputStream cpuUtilization;

    /** The timer. */
    private static Timer timer = new Timer(true);

    // This cpuUtilTask will enter a line in the cpuUtilization file every timer
    // period.
    // The line indicates the CPU utilization with a cheesy ASCII histogram:
    //
    // jbosadmin@dev-eti-gpu-1:pts/0├────> cat -f cpu_utilization.txt
    // 1.X
    // 3...X
    // 3...X
    // 3...X
    // 6......X
    // 3...X
    // 9.........X
    // 13.............X
    // That indicates that over a number of period checks, the highest
    // utilization
    // in that above sampling is 13%.
    //
    // NOTE: each line is purposely not flushed! So, tailing would be boring.
    // The cpuUtilization file is flushed and closed when rotated or program
    // exit.
    /** The cpu util task. */
    private static TimerTask cpuUtilTask;

    static {
        boolean measureCpuUtilReport = CONVERTER_LEVEL_LOG >= LOG_LEVEL_TELEMETRY;
        measureCpuUtilReport = measureCpuUtilReport
                && Boolean.getBoolean(State.class.getName() + CPU_UTIL_REPORT_EXTENSION);
        if (measureCpuUtilReport) {
            try {
                cpuUtilization =
                        new DataOutputStream(
                                new BufferedOutputStream(new FileOutputStream(CPU_UTIL_PATH),
                                        64 * 1024));

                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        closeCpuReport();
                    }
                });
                cpuUtilTask = new CpuUtilTask();
                timer.schedule(cpuUtilTask, 25, 25);
            } catch (Exception e) {
                throw new TiffToScaledPngException("Failed to open cpu utilization report, "
                        + CPU_UTIL_PATH, e);
            }
        }
    }

    /**
     * One time initialization operation within the native shared library.
     * <p/>
     * <b>Harping:</b> some native static initialization is based upon this
     * class's class (static) fields that are reflectively read in the native
     * code!
     *
     * @return the int
     */
    /* package for test */
    static native int initStatics();

    /** The Constant HAS_GPU. */
    private static final boolean HAS_GPU;

    /** The Constant JAVA_LIB_PATH. */
    private static final String JAVA_LIB_PATH = "java.library.path";

    static {
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("LD_LIBRARY_PATH: " + System.getProperty(JAVA_LIB_PATH));
            }
            System.loadLibrary(NATIVE_SHARED_LIB_NAME);
            int hasGpuInt = initStatics();
            HAS_GPU = hasGpuInt == 1;
            report = initReport();
        } catch (TiffToScaledPngException ttspe) {
            throw ttspe;
        } catch (Throwable t) {
            LOGGER.error("LD_LIBRARY_PATH: " + System.getProperty(JAVA_LIB_PATH));
            throw new TiffToScaledPngException("Failed to initialize report, native statics or load native shared library:"
                    + NATIVE_SHARED_LIB_NAME, new Exception(t));
        }
    }

    /* State that represents input args to native code. */
    /** The requested resource. */
    private final String requestedResource;

    /** The tiff image. */
    private final byte[] tiffImage;

    /** The png width. */
    private final int pngWidth;

    /** The image chain. */
    private final int imageChain;

    /*
     * State that represents output args from the native code. IOW: reflectively
     * set in the native code.
     */
    /** The png image. */
    private final byte[] pngImage = new byte[0];

    /* Telemetry set by native code via reflection. */
    /** The tiff width. */
    private int tiffWidth;

    /** The tiff height. */
    private int tiffHeight;

    /** The tiff compressed size. */
    private final int tiffCompressedSize;

    /** The total native time. */
    private double totalNativeTime;

    /** The image convert time. */
    private double imageConvertTime;

    /** The image decompress time. */
    private double imageDecompressTime;

    /** The image scale time. */
    private double imageScaleTime;

    /** The image encode png time. */
    private double imageEncodePngTime;

    /** The gpu device. */
    private int gpuDevice;

    /** The gpu wait time. */
    private double gpuWaitTime;

    /** The png image size. */
    private int pngImageSize;

    /** The png height. */
    private int pngHeight;

    /**
     * This ctor requires the minimum arguments to perform a native conversion.
     *
     * @param imageChain
     *         the image chain
     * @param requestedResource
     *         used for native logging and native exception messages
     * @param tiffImage
     *         TIFF to be scaled and converted into a PNG.
     * @param pngWidth
     *         width to scale image prior to PNG compression.
     */
    @edu.umd.cs.findbugs.annotations.SuppressWarnings(
            value = "EI_EXPOSE_REP2",
            justification = "TIFF image data is very large; to large to copy. Though mutable is never changed elsewhere.")
    public State(ImageChain imageChain, String requestedResource, byte[] tiffImage, int pngWidth) {
        this.requestedResource = requestedResource;
        this.tiffImage = tiffImage;
        this.pngWidth = pngWidth;
        this.imageChain = imageChain.getJniConstant();

        if (tiffImage == null || pngWidth < 2) {
            StringBuilder sb = new StringBuilder(requestedResource);
            if (tiffImage == null) {
                sb.append(" has no TIFF image data.");
            } else {
                sb.append(" scale width is too small: ")
                        .append(pngWidth).append("px.");
            }
            throw new TiffToScaledPngException(sb.toString());
        }
        tiffCompressedSize = tiffImage.length;
    }

    /**
     * Gets the png image.
     *
     * @return the png image
     */
    public byte[] getPngImage() {
        return pngImage;
    }

    /**
     * Checks if is checks for gpu.
     *
     * @return true, if is checks for gpu
     */
    public static boolean isHasGpu() {
        return HAS_GPU;
    }

    /**
     * Gets the tiff width.
     *
     * @return the tiff width
     */
    public int getTiffWidth() {
        return tiffWidth;
    }

    /**
     * Gets the tiff height.
     *
     * @return the tiff height
     */
    public int getTiffHeight() {
        return tiffHeight;
    }

    /**
     * Gets the total native time.
     *
     * @return the total native time
     */
    public double getTotalNativeTime() {
        return totalNativeTime;
    }

    /**
     * Gets the image convert time.
     *
     * @return the image convert time
     */
    public double getImageConvertTime() {
        return imageConvertTime;
    }

    /**
     * Gets the image decompress time.
     *
     * @return the image decompress time
     */
    public double getImageDecompressTime() {
        return imageDecompressTime;
    }

    /**
     * Gets the image scale time.
     *
     * @return the image scale time
     */
    public double getImageScaleTime() {
        return imageScaleTime;
    }

    /**
     * Gets the gpu device.
     *
     * @return the gpu device
     */
    public int getGpuDevice() {
        return gpuDevice;
    }

    /**
     * Gets the gpu wait time.
     *
     * @return the gpu wait time
     */
    public double getGpuWaitTime() {
        return gpuWaitTime;
    }

    /**
     * Gets the image encode png time.
     *
     * @return the image encode png time
     */
    public double getImageEncodePngTime() {
        return imageEncodePngTime;
    }

    /**
     * Gets the png height.
     *
     * @return the png height
     */
    public int getPngHeight() {
        return pngHeight;
    }

    /**
     * Gets the png width.
     *
     * @return the png width
     */
    public int getPngWidth() {
        return pngWidth;
    }

    /**
     * Persist a single converts telemetry to the CSV report.
     *
     * @param host
     *         the host
     * @param sourcePath
     *         the source path
     * @param start
     *         the start
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    public void updateReport(String host, String sourcePath, long start) throws IOException {
        if (CONVERTER_LEVEL_LOG < LOG_LEVEL_TELEMETRY) {
            return;
        }
        try {
            long tiffImageSize = tiffWidth * tiffHeight;
            long pngImageSize = pngWidth * pngHeight;
            float difference = (((float) pngImageSize - tiffImageSize) / tiffImageSize) * 100.0f;
            double nativeOverhead = totalNativeTime - imageConvertTime;
            long requestTime = System.currentTimeMillis() - start;
            double javaOverhead = requestTime - totalNativeTime;
            REPORT_WRITE_LOCK.lock();
            report.append(String.format("%s,%s,%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%.1f,%.3f,%.3f,%d,%.3f,%.3f,%.3f,%d,%.3f%n",
                    REPORT_DATE_TIME_FORMAT.format(new Date()),
                    host,
                    sourcePath,
                    ImageChain.getImageChain(imageChain).getSuffix(),
                    tiffWidth,
                    tiffHeight,
                    tiffImageSize,
                    tiffCompressedSize,
                    pngWidth,
                    pngHeight,
                    pngImageSize,
                    pngImage.length,
                    difference,
                    imageDecompressTime,
                    imageScaleTime,
                    gpuDevice,
                    imageEncodePngTime,
                    imageConvertTime,
                    nativeOverhead,
                    requestTime,
                    javaOverhead));
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to update report.", e);
        } finally {
            REPORT_WRITE_LOCK.unlock();
        }
    }

    /**
     * Update image chain report.
     *
     * @param report
     *         the report
     * @param host
     *         the host
     * @param start
     *         the start
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    public void updateImageChainReport(Writer report, String host, long start) throws IOException {
        if (CONVERTER_LEVEL_LOG < LOG_LEVEL_TELEMETRY) {
            return;
        }
        try {
            long tiffImageSize = tiffWidth * tiffHeight;
            long pngImageSize = pngWidth * pngHeight;
            REPORT_WRITE_LOCK.lock();
            report.append(String.format("%s,%s,%s,%d,%d,%d,%d,%d,%d,%.3f,%.3f,%d,%.3f,%.3f%n",
                    REPORT_DATE_TIME_FORMAT.format(new Date()),
                    host,
                    ImageChain.getImageChain(imageChain).getSuffix(),
                    tiffWidth,
                    tiffHeight,
                    tiffImageSize,
                    pngWidth,
                    pngHeight,
                    pngImageSize,
                    imageDecompressTime,
                    imageScaleTime,
                    gpuDevice,
                    imageEncodePngTime,
                    imageConvertTime));
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to update report.", e);
        } finally {
            REPORT_WRITE_LOCK.unlock();
        }
    }

    /**
     * Read & Rotate copies the current report to a timestamped file and creates
     * a new, clean report for future conversion telemetry. The content of the
     * original report is returned.
     */
    private static final String CHAR_SET_NAME = "ISO-8859-1";

    /** The Constant DT_FORMAT. */
    private static final String DT_FORMAT = "MMddHHmmss";

    /**
     * Read and rotate report.
     *
     * @return the string
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    @edu.umd.cs.findbugs.annotations.SuppressWarnings(
            value = "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE",
            justification = "Don't care if file previously existed or not."
    )
    public static String readAndRotateReport() throws IOException {
        if (CONVERTER_LEVEL_LOG < LOG_LEVEL_TELEMETRY) {
            return null;
        }
        closeReport();

        // Copy report to timestamped file.
        DateFormat format = new SimpleDateFormat(DT_FORMAT);
        File reportFile = new File(REPORT_CSV_PATH);
        String timestamp = format.format(new Date());
        String csvExtension = ".csv";
        String copyFileName =
                REPORT_CSV_PATH.substring(0, REPORT_CSV_PATH.lastIndexOf(csvExtension)) + PATH_SEPERATER + timestamp
                        + csvExtension;
        File copy = new File(copyFileName);
        copyReport(reportFile, copy);

        // Delete original and create anew.
        reportFile.delete();
        report = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(reportFile, false), CHAR_SET_NAME));
        report.append(REPORT_HEADER);
        report.flush();

        // Return the text of the copied original.
        return readTextFile(copy);
    }

    /** The Constant PATH_SEPERATER. */
    private static final String PATH_SEPERATER = "_";

    /**
     * Read & Rotate copies the current CPU utilization report to a timestamped
     * file and creates a new, clean report for future conversion telemetry. The
     * content of the original report is returned.
     *
     * @return text of original report.
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    @edu.umd.cs.findbugs.annotations.SuppressWarnings(
            value = "RV_RETURN_VALUE_IGNORED_BAD_PRACTICE",
            justification = "Don't care if file previously existed or not."
    )
    public static String readAndRotateCpuUtilReport() throws IOException {

        boolean measureCpuUtilReport = CONVERTER_LEVEL_LOG >= LOG_LEVEL_TELEMETRY;
        measureCpuUtilReport = measureCpuUtilReport
                && Boolean.getBoolean(State.class.getName() + CPU_UTIL_REPORT_EXTENSION);
        if (!measureCpuUtilReport) {
            return null;
        }

        cpuUtilTask.cancel();
        closeCpuReport();

        // Copy report to timestamped file.
        DateFormat format = new SimpleDateFormat(DT_FORMAT);
        File cpuUtilReport = new File(CPU_UTIL_PATH);
        String timestamp = format.format(new Date());
        String binExtension = ".bin";
        String copyFileName =
                CPU_UTIL_PATH.substring(0, CPU_UTIL_PATH.lastIndexOf(binExtension)) + PATH_SEPERATER + timestamp + binExtension;
        File copy = new File(copyFileName);
        copyReport(cpuUtilReport, copy);

        // Delete original and create anew.
        cpuUtilReport.delete();
        cpuUtilization = new DataOutputStream(
                new BufferedOutputStream(new FileOutputStream(CPU_UTIL_PATH, true),
                        64 * 1024));
        cpuUtilTask = new CpuUtilTask();
        timer.schedule(cpuUtilTask, 25, 25);

        // Return the text of the copied original.
        return readCpuFile(copy);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "State{" + (tiffImage != null ? "Has TIFF image data" : "No TIFF image data")
                + ", requestedResource=" + requestedResource
                + ", tiffWidth=" + tiffWidth
                + ", tiffHeight=" + tiffHeight
                + ", imageConvertTime=" + imageConvertTime
                + ", imageDecompressTime=" + imageDecompressTime
                + ", imageScaleTime=" + imageScaleTime
                + ", gpuDevice=" + gpuDevice
                + ", imageEncodePngTime=" + imageEncodePngTime
                + (pngImage != null ? ", Has PNG image data" : ", No PNG image data")
                + ", pngImageSize=" + (pngImage != null ? pngImage.length : 0)
                + ", pngWidth=" + pngWidth
                + ", pngHeight=" + pngHeight
                + '}';
    }

    /**
     * Inits the report.
     *
     * @return the writer
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static Writer initReport() throws IOException {
        if (CONVERTER_LEVEL_LOG < LOG_LEVEL_TELEMETRY) {
            return null;
        }
        File reportFile = new File(REPORT_CSV_PATH);
        Writer report = null;
        try {
            boolean append = reportFile.exists();
            report = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(reportFile, append), CHAR_SET_NAME), 64 * 1024);
            if (!append) {
                report.append(REPORT_HEADER);
            }

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    closeReport();
                }
            });

        } catch (IOException e) {
            throw new TiffToScaledPngException("Failed to create report file: " + reportFile.getAbsolutePath(), e);
        }

        return report;
    }

    /**
     * Close report.
     */
    private static void closeReport() {
        try {
            if (report != null) {
                report.close();
            }
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to close report.", e);
        }
    }

    /**
     * Close cpu report.
     */
    private static void closeCpuReport() {
        try {
            if (cpuUtilization != null) {
                cpuUtilization.close();
            }
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to close report.", e);
        }
    }

    /** Read text file. */
    private static final String EXCEPTION_MESSAGE = "Report is >= 2 GB";

    /**
     * Read text file.
     *
     * @param file
     *         the file
     * @return the string
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static String readTextFile(File file) throws IOException {
        DataInputStream in = null;
        try {
            in = new DataInputStream(new FileInputStream(file));
            long longlength = file.length();
            int length = (int) longlength;
            if (length != longlength) {
                throw new TiffToScaledPngException(EXCEPTION_MESSAGE);
            }

            byte[] data = new byte[length];
            in.readFully(data);
            return new String(data, "UTF-8");
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /** The Constant IS_TESTING. */
    private static final String IS_TESTING = ".isTesting";

    /** The Constant CSV_HEADER. */
    private static final String CSV_HEADER = Boolean.getBoolean(State.class.getName() + IS_TESTING)
            ? "Time (MMdd:HH:mm:ss), Time Since Start (msec), CPU Utilization (%), Time to Measure Utilization(ms)\n"
            : "Time (MMdd:HH:mm:ss), Time Since Start (msec), CPU Utilization (%)\n";

    /** The Constant STR_LENGTH_TO_HOLD_TIME. */
    private static final int STR_LENGTH_TO_HOLD_TIME;

    /** The date format. */
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd:HH:mm:ss");

    static {
        int holdTimeSize = dateFormat.format(new Date()).length();
        long msecsInSec = 1000;
        long secondsInMinute = 60;
        long minutesInHour = 60;
        long hoursInDay = 24;
        long msecsInOneDay = msecsInSec * secondsInMinute * minutesInHour * hoursInDay;
        int holdMsecInOneDay = Long.toString(msecsInOneDay).length();
        STR_LENGTH_TO_HOLD_TIME = holdTimeSize + ",".length()
                + holdMsecInOneDay;
    }

    /**
     * Read cpu file.
     *
     * @param file
     *         the file
     * @return the string
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    @edu.umd.cs.findbugs.annotations.SuppressWarnings(
            value = "STCAL_INVOKE_ON_STATIC_DATE_FORMAT_INSTANCE",
            justification = "Don't care if file previously existed or not."
    )
    private static String readCpuFile(File file) throws IOException {
        DataInputStream in = null;
        StringBuffer sb = null;
        boolean isTesting = Boolean.getBoolean(State.class.getName() + IS_TESTING);
        REPORT_WRITE_LOCK.lock();
        try {
            in = new DataInputStream(new FileInputStream(file));
            long longlength = file.length();
            int length = (int) longlength;
            if (length != longlength) {
                throw new TiffToScaledPngException(EXCEPTION_MESSAGE);
            }
            double bytesPerFileEntry = isTesting ? 18.0d : 17.0d;
            int entryCnt = (int) Math.ceil(length / bytesPerFileEntry);
            String cpuUtilizationStr = isTesting ? ",99,99\n" : ",99\n";
            int charsPerTextEntry = entryCnt * (STR_LENGTH_TO_HOLD_TIME + cpuUtilizationStr.length());
            sb = new StringBuffer(CSV_HEADER.length() + charsPerTextEntry + 10);
            sb.append(CSV_HEADER);
            for (int ii = 0; ii < entryCnt; ii++) {
                dateFormat.format(in.readLong(), sb, DontCareFieldPosition.INSTANCE);
                sb.append(',').append(in.readLong()).append(',')
                        .append(in.readUnsignedByte());
                if (isTesting) {
                    sb.append(',').append(in.readUnsignedByte());
                }
                sb.append('\n');
            }
            return sb.toString();
        } catch (EOFException eof) {
            return eof.toString();

        } finally {
            REPORT_WRITE_LOCK.unlock();
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * The Class DontCareFieldPosition.
     */
    private static class DontCareFieldPosition extends FieldPosition {
        // The singleton of DontCareFieldPosition.
        /** The Constant INSTANCE. */
        static final FieldPosition INSTANCE = new DontCareFieldPosition();

        /**
         * Formatted.
         *
         * @param attr
         *         the attr
         * @param value
         *         the value
         * @param start
         *         the start
         * @param end
         *         the end
         * @param buffer
         *         the buffer
         */
        public void formatted(Format.Field attr, Object value, int start,
                              int end, StringBuffer buffer) {
        }

        /**
         * Formatted.
         *
         * @param fieldID
         *         the field id
         * @param attr
         *         the attr
         * @param value
         *         the value
         * @param start
         *         the start
         * @param end
         *         the end
         * @param buffer
         *         the buffer
         */
        public void formatted(int fieldID, Format.Field attr, Object value,
                              int start, int end, StringBuffer buffer) {
        }

        /**
         * Instantiates a new dont care field position.
         */
        private DontCareFieldPosition() {
            super(0);
        }
    }

    /**
     * Copy report.
     *
     * @param sourceReport
     *         the source report
     * @param destinationReport
     *         the destination report
     * @throws IOException
     *         Signals that an I/O exception has occurred.
     */
    private static void copyReport(File sourceReport, File destinationReport) throws IOException {
        if (!destinationReport.exists()) {
            boolean created = destinationReport.createNewFile();
            if (!created) {
                throw new RuntimeException("Couldn't create report file.");
            }
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceReport).getChannel();
            destination = new FileOutputStream(destinationReport).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    /* package for test */

    /**
     * Sets the native debug level.
     */
    static void setNativeDebugLevel() {
        CONVERTER_LEVEL_LOG = LOG_LEVEL_NONE;
        if (LOGGER.isTraceEnabled()) {
            CONVERTER_LEVEL_LOG = LOG_LEVEL_TRACE;
        } else if (LOGGER.isDebugEnabled()) {
            CONVERTER_LEVEL_LOG = LOG_LEVEL_DEBUG;
        } else if (LOGGER.isInfoEnabled()) {
            CONVERTER_LEVEL_LOG = LOG_LEVEL_TELEMETRY;
        } else {
            CONVERTER_LEVEL_LOG = LOG_LEVEL_NONE;
        }
        LOGGER.info("Native log level: " + CONVERTER_LEVEL_LOG);
    }

    /**
     * The Class CpuUtilTask.
     */
    private static class CpuUtilTask extends TimerTask {

        /** The start. */
        private final long start = System.currentTimeMillis();

        /** The prior cpu times. */
        private CpuTimes priorCpuTimes = new JavaSysMon().cpuTimes();

        /** The is testing. */
        private final boolean isTesting = Boolean.getBoolean(State.class.getName() + IS_TESTING);

        /*
         * (non-Javadoc)
         *
         * @see java.util.TimerTask#run()
         */
        @Override
        public void run() {
            long totalRunStart = 0;
            if (isTesting) {
                totalRunStart = System.currentTimeMillis();
            }
            CpuTimes cpuTimes = new JavaSysMon().cpuTimes();
            try {
                int cpuUsage = (int) (cpuTimes.getCpuUsage(priorCpuTimes) * 100L);
                if (cpuUsage > 30 || isTesting) {
                    cpuUtilization.writeLong(System.currentTimeMillis());
                    cpuUtilization.writeLong(System.currentTimeMillis() - start);
                    cpuUtilization.writeByte(Math.min(99, cpuUsage));
                    if (isTesting) {
                        cpuUtilization.writeByte((int) Math.min(99, System.currentTimeMillis() - totalRunStart));
                    }
                }
            } catch (Exception e) {
                throw new TiffToScaledPngException("Failed to update cpu utilization report, "
                        + CPU_UTIL_PATH, e);
            }
            priorCpuTimes = cpuTimes;
        }
    }
}
