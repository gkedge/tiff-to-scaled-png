package com.kedges.tiffscaledpng;

/**
 * gkedge - 1/9/14.
 */
public class TiffToScaledPngException extends RuntimeException {

    /**
     * Instantiates a new tiff to scaled png exception.
     *
     * @param msg
     *         the msg
     */
    public TiffToScaledPngException(String msg) {
        super(msg);
    }

    /**
     * Instantiates a new tiff to scaled png exception.
     *
     * @param msg
     *         the msg
     * @param e
     *         the e
     */
    public TiffToScaledPngException(String msg, Exception e) {
        super(msg, e);
    }
}
