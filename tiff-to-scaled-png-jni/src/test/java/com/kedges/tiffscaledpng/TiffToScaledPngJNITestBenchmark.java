package com.kedges.tiffscaledpng;

import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import com.carrotsearch.junitbenchmarks.BenchmarkRule;
import com.carrotsearch.junitbenchmarks.annotation.*;
import com.jezhumble.javasysmon.JavaSysMon;
import org.junit.*;

import java.io.IOException;
import java.io.RandomAccessFile;

import static org.fest.assertions.Assertions.assertThat;

/**
 * This unit test will provide single run benchmark AND a persisted history
 * report. The file name of the single- run report is named in the @BenchmarkHistoryChart
 * annotation (with .html suffix). It is overwritten each time.
 * <p/>
 * The file name of the persisted history report is the fully qualified class
 * name of the of the UT (with .html suffix). It is overwritten with each time
 * with the current run added to the history of data.
 * <p/>
 * The history is intended to be used to compare different conversion algorithms
 * like Lept vs some other native implementation or perhaps Lept optimally
 * recompiled.
 */

@AxisRange(min = 0, max = 0.1)
// Name of the single-run report (with .html suffix):
@BenchmarkMethodChart(filePrefix = "tiff-to-scaled-png-scaled-single-image")
@BenchmarkHistoryChart(labelWith = LabelType.CUSTOM_KEY, maxRuns = 10)
@BenchmarkOptions(concurrency = BenchmarkOptions.CONCURRENCY_SEQUENTIAL,
        benchmarkRounds = 5, warmupRounds = 3)
public class TiffToScaledPngJNITestBenchmark {
    static {
        System.out.println("User directory: " + System.getProperty("user.dir"));
    }

    private final static double targetFrequency =
            new JavaSysMon().cpuFrequencyInHz();

    static {
        // Persist benchmark data to an H2 data base.
        System.setProperty("jub.consumers", "CONSOLE,H2");
        String gigiHz = Double.toString(Math.round(targetFrequency / 10000000.0d) / 100.0d);
        // X-axis label
        System.setProperty("jub.customkey", "Lept(" + gigiHz + "GHz)");
        System.setProperty("jub.db.file", "play-single-image.benchmarks");
        // System.setProperty("jub.db.file", "real-single-image.benchmarks");
    }

    //    private static final String TEST_IMAGE = "tiff-to-scaled-png-jni/src/test/resources/testImages/PTO/TestImage14.tif";
    private static final String TEST_IMAGE = "src/test/resources/testImages/PTO/TestImage14.tif";

    @Rule
    public org.junit.rules.TestRule benchmarkRun = new BenchmarkRule();

    private static byte[] tiffData = new byte[0];

    @BeforeClass
    public static void setUp() throws IOException {

        double baselineFrequency = 3.392E9; // For a machine with this freq...
        double baselineTime = 0.05d; // ...test image is converted in this secs.
        double numerator = baselineFrequency * baselineTime;
        System.out.println("Target Frequency: " + targetFrequency);
        System.out.println("Expected time: " + (numerator / targetFrequency));
        // Read in a TIFF test file and convert it into a PNG.
        RandomAccessFile tiffTestFile = new RandomAccessFile(TEST_IMAGE, "r");
        try {
            // Get and check length
            long longlength = tiffTestFile.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read TIFF into buffer set in State.
            tiffData = new byte[length];
            tiffTestFile.readFully(tiffData);
        } finally {
            tiffTestFile.close();
        }
    }

    @Test
    public void tiffToScaledPng_100w() throws IOException {
        long start = System.currentTimeMillis();

        State state = new State(State.ImageChain.LEPTONICA, TEST_IMAGE, tiffData, 100);
        TiffToScaledPngJNI.convertTiffToScaledPng(state);
        byte[] pngImage = state.getPngImage();

        // Make sure the PNG data starts with the PNG's
        // magic file signature.
        assertThat(pngImage).contains(new byte[]{(byte) 0x89, 0x50, 0x4E, 0x47,
                0x0D, 0x0A, 0x1A, 0x0A});

        state.updateReport("Host", TEST_IMAGE, start);
    }

    @Test
    public void tiffToScaledPng_816w() throws IOException {
        long start = System.currentTimeMillis();

        State state = new State(State.ImageChain.LEPTONICA, TEST_IMAGE, tiffData, 816);
        TiffToScaledPngJNI.convertTiffToScaledPng(state);
        byte[] pngImage = state.getPngImage();

        // Make sure the PNG data starts with the PNG's
        // magic file signature.
        assertThat(pngImage).contains(new byte[]{(byte) 0x89, 0x50, 0x4E, 0x47,
                0x0D, 0x0A, 0x1A, 0x0A});

        state.updateReport("Host", TEST_IMAGE, start);
    }

    @Test
    public void tiffToScaledPng_2000w() throws IOException {
        long start = System.currentTimeMillis();

        State state = new State(State.ImageChain.LEPTONICA, TEST_IMAGE, tiffData, 2000);
        TiffToScaledPngJNI.convertTiffToScaledPng(state);
        byte[] pngImage = state.getPngImage();

        // Make sure the PNG data starts with the PNG's
        // magic file signature.
        assertThat(pngImage).contains(new byte[]{(byte) 0x89, 0x50, 0x4E, 0x47,
                0x0D, 0x0A, 0x1A, 0x0A});

        state.updateReport("Host", TEST_IMAGE, start);
    }

    @Test
    public void tiffToScaledPng_3000w() throws IOException {
        long start = System.currentTimeMillis();

        State state = new State(State.ImageChain.LEPTONICA, TEST_IMAGE, tiffData, 3000);
        TiffToScaledPngJNI.convertTiffToScaledPng(state);
        byte[] pngImage = state.getPngImage();

        // Make sure the PNG data starts with the PNG's
        // magic file signature.
        assertThat(pngImage).contains(new byte[]{(byte) 0x89, 0x50, 0x4E, 0x47,
                0x0D, 0x0A, 0x1A, 0x0A});

        state.updateReport("Host", TEST_IMAGE, start);
    }

}
