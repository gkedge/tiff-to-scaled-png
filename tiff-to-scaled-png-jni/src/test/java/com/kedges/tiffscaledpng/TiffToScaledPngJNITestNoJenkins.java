package com.kedges.tiffscaledpng;

import com.kedges.tiffscaledpng.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * gkedge - 1/9/14
 */

public class TiffToScaledPngJNITestNoJenkins {

    static {
        System.out.println("User directory: " + System.getProperty("user.dir"));
    }

    public static final DateFormat REPORT_DATE_TIME_FORMAT = new SimpleDateFormat("MMdd:HH:mm:ss:SSS");
    //    private static final String TEST_IMAGE = "tiff-to-scaled-png-jni/src/test/resources/testImages/10by15HorizontalAlternating.tif";
    private static final String TEST_IMAGE = "src/test/resources/testImages/10by15HorizontalAlternating.tif";
    private static final String LEPTONICA_REPORT_CSV_PATH = "leptonica_report_tiff_to_scaled_png.csv";
    private static final String OPENCV_REPORT_CSV_PATH = "opencv_report_tiff_to_scaled_png.csv";
    private static Writer leptonicaReport;
    private static Writer opencvReport;

    private static final Map<String, byte[]> pathToFileContentMap = new HashMap<String, byte[]>();

    public static final String REPORT_HEADER = "Time(MMdd:HH:mm:ss:SSS),Host,Conversion Path," +
            "Src Width(px),Src Height(px),Src Size(px)," +
            "Dest Width(px),Dest Height(px),Dest Size(px)," +
            "TIFF Decompress(ms),Scale(ms),PNG encode(ms),Total Image Conversion(ms)\n";
    private static final File testImageDirectory = new File(System.getProperty("java.io.tmpdir") + File.separator
            + "ImageConversions");

    // @Test
    // public void testConvertTiffToScaledPng() throws IOException {
    // convertAndValidatePng();
    // }

    /*
     * It would be nice to test all the options of debug to make sure there
     * isn't any indirect failure due to the debug level. But, a dynamic ability
     * to change level on the fly would be needed. Today, every debug related
     * setting is now staticly retained. For now, any changes should be tested
     * by altering the level manually to each level and rerun the test. Sorry.
     *
     * @Test public void testConvertTiffToScaledPng_WARN() throws IOException {
     * Logger.getLogger(State.class).setLevel(Level.WARN);
     * convertAndValidatePng(); }
     *
     * @Test public void testConvertTiffToScaledPng_INFO() throws IOException {
     * Logger.getLogger(State.class).setLevel(Level.INFO);
     * convertAndValidatePng(); }
     *
     * @Test public void testConvertTiffToScaledPng_DEBUG() throws IOException {
     * Logger.getLogger(State.class).setLevel(Level.DEBUG);
     * convertAndValidatePng(); }
     *
     * @Test public void testConvertTiffToScaledPng_TRACE() throws IOException {
     * Logger.getLogger(State.class).setLevel(Level.TRACE);
     * convertAndValidatePng(); }
     */

    public void copyTestImages() throws IOException {
//        File testImageDirectorySrc = new File("tiff-to-scaled-png-jni/src/test/resources/testImages/PTO");
        File testImageDirectorySrc = new File("src/test/resources/testImages/PTO");
        FileUtils.forceMkdir(testImageDirectory);
        FileUtils.copyDirectory(testImageDirectorySrc, testImageDirectory);

        String[] list = testImageDirectory.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".tif");
            }
        });

        for (String tiffFileName : list) {
            String subdirectory = tiffFileName.substring(0, tiffFileName.lastIndexOf(".tif"));
            String pathname = String.format("%s%s%s", testImageDirectory, File.separator, subdirectory);
            File directory = new File(pathname);
            FileUtils.forceMkdir(directory);
        }
    }

    @Test
    public void testRaster() throws IOException {
//        String TEST_IMAGE =
//                "tiff-to-scaled-png-jni/src/test/resources/testImages/10by15HorizontalAlternating.tif";
        String TEST_IMAGE =
                "src/test/resources/testImages/10by15HorizontalAlternating.tif";
        for (int ii = 8; ii < 16; ii++) {
//            convertTiffToPng(120,
//                    "tiff-to-scaled-png-jni/src/test/resources/testImages/10by" + ii + "HorizontalAlternating.tif",
//                    State.ImageChain.LEPTONICA,
//                    "10by" + ii + "15HorizontalAlternating.png");
            convertTiffToPng(120,
                    "src/test/resources/testImages/10by" + ii + "HorizontalAlternating.tif",
                    State.ImageChain.LEPTONICA,
                    "10by" + ii + "15HorizontalAlternating.png");
        }
    }

    // Make sure the PNG data starts with the PNG's
    // magic file signature.
    /*
     * byte[] pngImage = state.getPngImage();
     *
     * assertThat(pngImage).contains(new byte[]{(byte) 0x89, 0x50, 0x4E, 0x47,
     * 0x0D, 0x0A, 0x1A, 0x0A});
     *
     * RandomAccessFile goldenPngFile = new
     * RandomAccessFile("tiff-to-scaled-png-jni/src/test/resources/Test.png",
     * "r"); try { // Get and check length long longlength =
     * goldenPngFile.length(); int length = (int) longlength; if (length !=
     * longlength) throw new IOException("File size >= 2 GB");
     * System.out.println("PNG file size: " + length); // Read golden PNG into
     * buffer set in State. byte[] goldenPngData = new byte[length];
     * goldenPngFile.readFully(goldenPngData);
     *
     * assertThat(goldenPngData).isEqualTo(pngImage); } finally {
     * goldenPngFile.close(); } }
     */

    private static State convertTiffToPng(int scaledWidth, String tiffFileName,
                                          State.ImageChain imageChain,
                                          String compressedFileName) throws IOException {
        byte[] data = pathToFileContentMap.get(tiffFileName);

        if (data == null) {
            RandomAccessFile tiffTestFile = new RandomAccessFile(tiffFileName, "r");
            // Get and check length
            long longlength = tiffTestFile.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");

            // Read TIFF into buffer set in State.
            data = new byte[length];
            tiffTestFile.readFully(data);

            pathToFileContentMap.put(tiffFileName, data);
        }

        State state = new State(State.ImageChain.OPENCV, tiffFileName, data, scaledWidth);
        TiffToScaledPngJNI.convertTiffToScaledPng(state);

        RandomAccessFile compressedFile = new RandomAccessFile(compressedFileName, "rw");
        compressedFile.write(state.getPngImage());
        compressedFile.close();
        return state;
    }

    private static void updateImageChainReport(Writer report, String host, int imageConversionPath,
                                               int tiffWidth, int tiffHeight, int pngWidth, int pngHeight,
                                               double imageDecompressTime, double imageScaleTime,
                                               double imageEncodePngTime, double imageConvertTime) throws IOException {
        try {
            long tiffImageSize = tiffWidth * tiffHeight;
            long pngImageSize = pngWidth * pngHeight;
            report.append(String.format("%s,%s,%s,%d,%d,%d,%d,%d,%d,%.3f,%.3f,%.3f,%.3f\n",
                    REPORT_DATE_TIME_FORMAT.format(new Date()),
                    host,
                    imageConversionPath == 0 ? "OpenCV" : imageConversionPath == 1 ?
                            "OpenCV-CUDA" : imageConversionPath == 2 ? "Leptonica" : "Leptonica-CUDA",
                    tiffWidth,
                    tiffHeight,
                    tiffImageSize,
                    pngWidth,
                    pngHeight,
                    pngImageSize,
                    imageDecompressTime,
                    imageScaleTime,
                    imageEncodePngTime,
                    imageConvertTime
            ));
            // report.flush();
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to update report.", e);
        }
    }

    private static Writer initReport(File reportFile) throws IOException {
        Writer report = null;
        try {
            boolean append = reportFile.exists();
            report = new BufferedWriter(new FileWriter(reportFile, append), 64 * 1024);
            if (!append) {
                report.append(REPORT_HEADER);
            }

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    closeReports();
                }
            });

        } catch (IOException e) {
            throw new TiffToScaledPngException("Failed to create report file: " + reportFile.getAbsolutePath(), e);
        }

        return report;
    }

    private static void closeReports() {
        try {
            if (leptonicaReport != null) {
                leptonicaReport.close();
                leptonicaReport = null;
            }
            if (opencvReport != null) {
                opencvReport.close();
                opencvReport = null;
            }
        } catch (Exception e) {
            throw new TiffToScaledPngException("Failed to close report.", e);
        }
    }

}
