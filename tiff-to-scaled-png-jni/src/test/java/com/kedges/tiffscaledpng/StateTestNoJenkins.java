package com.kedges.tiffscaledpng;

import org.junit.Test;

import java.io.IOException;
import java.io.RandomAccessFile;

import static org.fest.assertions.Assertions.assertThat;

/**
 * gkedge - 1/9/14
 */
public class StateTestNoJenkins {
    static {
        System.out.println("User directory: " + System.getProperty("user.dir"));
        System.setProperty(State.class.getName() + ".CpuUtilReport", "true");
        System.setProperty(State.class.getName() + ".isTesting", "true");
    }

//        private static final String TEST_IMAGE = "tiff-to-scaled-png-jni/src/test/resources/testImages/PTO/TestImage14.tif";
    private static final String TEST_IMAGE = "src/test/resources/testImages/PTO/TestImage14.tif";

    @Test
    public void testReadAndRotateReport() throws IOException {
        long startTime = System.currentTimeMillis();
        // Read in a TIFF test file and convert it into a PNG.
        RandomAccessFile f = new RandomAccessFile(TEST_IMAGE, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read TIFF into buffer set in State.
            byte[] data = new byte[length];
            f.readFully(data);
            State state = new State(State.ImageChain.OPENCV, TEST_IMAGE, data, 816);

            TiffToScaledPngJNI.convertTiffToScaledPng(state);
            byte[] pngImage = state.getPngImage();

            // Make sure the PNG data starts with the PNG's
            // magic file signature.
            assertThat(pngImage).contains(new byte[]{(byte) 0x89, 0x50, 0x4E, 0x47,
                    0x0D, 0x0A, 0x1A, 0x0A});

            state.updateReport("Test host", TEST_IMAGE, startTime);

        } finally {
            f.close();
        }
        System.out.println(State.readAndRotateReport());
    }

    @Test
    public void testReadAndRotateCpuUtilReport() throws IOException, InterruptedException {
        State state = new State(State.ImageChain.OPENCV, "", new byte[10], 100);
        Thread.sleep(250);
        System.out.println(State.readAndRotateCpuUtilReport());
    }

}
