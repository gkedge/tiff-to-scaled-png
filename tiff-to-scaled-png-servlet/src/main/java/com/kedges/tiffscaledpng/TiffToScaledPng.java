package com.kedges.tiffscaledpng;

import org.apache.http.HttpHost;
import org.apache.http.StatusLine;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * gkedge - 12/26/13
 */
public final class TiffToScaledPng extends HttpServlet {
    protected static final int N_THREADS = Runtime.getRuntime().availableProcessors();

    private static int IMAGE_NUMBER;

    private static final String PE2E_WEB_APP_SERVER_PORT_SYS_PROP = "PE2E_WEB_APP_SERVER_PORT";
    private static final String PE2E_WEB_APP_SERVER_HOST_SYS_PROP = "PE2E_WEB_APP_SERVER_HOST";

    private static final String PE2E_WEB_APP_SERVER_HOST =
            System.getProperty(PE2E_WEB_APP_SERVER_HOST_SYS_PROP, "dev-eti-app-1.etc.uspto.gov");
    private static final int PE2E_WEB_SERVER_PORT =
            Integer.getInteger(PE2E_WEB_APP_SERVER_PORT_SYS_PROP, 8080);

    // TODO: we must get the current employee number from the client as a query param.
    private static final String EMPLOYEE_NUMBER = "80326";

    // A typical TIFF image is around 2500px wide.  For now,
    // consider a request for a PNG larger than 6x 3000px
    // to be ridiculous.
    private static final int MAX_PNG_SCALE = 3000 * 6;

    private final CookieStore cookieStore = new BasicCookieStore();
    private final PoolingHttpClientConnectionManager connectionManager =
            new PoolingHttpClientConnectionManager();

    private final CloseableHttpClient httpClient =
            HttpClients.custom()
                    .setConnectionManager(connectionManager)
                    .setDefaultCookieStore(cookieStore)
                    .build();

    private Writer cpuUtilization;

    {
        try {
            cpuUtilization =
                    new BufferedWriter(new FileWriter("cpu_utilization.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        // Set up HTTP client to communicate with PE2E Web Server.
        connectionManager.setMaxTotal(N_THREADS);
        HttpHost devAppServer = new HttpHost(PE2E_WEB_APP_SERVER_HOST, PE2E_WEB_SERVER_PORT);
        connectionManager.setMaxPerRoute(new HttpRoute(devAppServer), N_THREADS);

        BasicClientCookie cookie = new BasicClientCookie("pe2e.current.employeeNumber", EMPLOYEE_NUMBER);
        cookie.setDomain(".uspto.gov");
        cookie.setPath("/");
        cookieStore.addCookie(cookie);
    }

    @Override
    public void destroy() {
    }

    @Override
    final protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pngWidthStr = req.getParameter("width");

        String requestURI = req.getRequestURI();
        if (pngWidthStr == null || pngWidthStr.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_PRECONDITION_FAILED,
                    "Missing legal width value.");
            return;
        }

        int pngWidth = getPngImageWidth(resp, pngWidthStr);
        if (pngWidth == Integer.MIN_VALUE) {
            return;
        }

        int code = convertTiffFromPe2eWebServer(requestURI, pngWidth, resp.getOutputStream());

        if (code > 300) {
            resp.sendError(code,
                    code == HttpServletResponse.SC_NOT_FOUND ? "TIFF does not exist on PE2E Web Server." :
                            code == HttpServletResponse.SC_BAD_REQUEST ? "Requested PNG is too large." : "I dunno...");
        }
    }

    /* package */ int convertTiffFromPe2eWebServer(String requestURI, int pngWidth,
                                                   OutputStream responseStream) throws IOException {
        CloseableHttpResponse response = null;
        int imageNumber = 0;
        try {
            long start = System.currentTimeMillis();
            URI pe2EWebServerURI = getPE2EWebServerURI(requestURI);
            response = httpClient.execute(new HttpGet(pe2EWebServerURI));
            InputStream tiffImageStream = getTiffInputStream(pe2EWebServerURI, response);
            if (tiffImageStream == null) {
                return HttpServletResponse.SC_NOT_FOUND;
            }

            State imageState =
                    convertTiffStreamToPng(tiffImageStream, responseStream, pngWidth);

            imageState.updateReport("host", requestURI, start);
        } catch (URISyntaxException e) {
            return HttpServletResponse.SC_BAD_REQUEST;
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return HttpServletResponse.SC_OK;
    }

    protected static State convertTiffStreamToPng(InputStream tiffImageStream, OutputStream pngImageStream,
                                                  int pngImageWidth) throws IOException {
        State imageState =
                getScaledPngFromTiff(tiffImageStream, pngImageWidth);

        byte[] pngImage = imageState.getPngImage();
        pngImageStream.write(pngImage);
        pngImageStream.flush();
        pngImageStream.close();
/*
        OutputStream testTiff = new FileOutputStream("Test.tiff");
        testTiff.write(imageState.getTiff_image());
        testTiff.close();
        OutputStream testPng = new FileOutputStream("Test.png");
        testPng.write(pngImage);
        testPng.close();
*/
        return imageState;
    }

    private static State getScaledPngFromTiff(InputStream tiffImage,
                                              int pngWidth) throws IOException {
        State imageState = null;

        try {
            int available = tiffImage.available();
            byte[] tiffImageBuf = new byte[available];
            int read = tiffImage.read(tiffImageBuf, 0, available);
            if (read != available) {
                return null;
            }
            imageState = new State(State.ImageChain.LEPTONICA, "Foo", tiffImageBuf, pngWidth);
            TiffToScaledPngJNI.convertTiffToScaledPng(imageState);
        } finally {
            tiffImage.close();
        }


        return imageState;
    }

    /* package */
    static URI getPE2EWebServerURI(String path) throws URISyntaxException {
        return new URIBuilder()
                .setScheme("http")
                .setHost(PE2E_WEB_APP_SERVER_HOST)
                .setPort(PE2E_WEB_SERVER_PORT)
                .setPath(path)
                .build();
    }

    private static InputStream getTiffInputStream(URI imageURI, CloseableHttpResponse response) throws IOException {
        StatusLine statusLine = response.getStatusLine();
        if (statusLine.getStatusCode() > 300) {
            return null;
        }
        return new ByteArrayInputStream(EntityUtils.toByteArray(response.getEntity()));
    }

    private static int getPngImageWidth(HttpServletResponse resp, String pngWidthStr) throws IOException {
        int pngWidth;
        try {
            pngWidth = Integer.valueOf(pngWidthStr.trim());
            if (pngWidth > MAX_PNG_SCALE) {
                resp.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE,
                        "Requested PNG is too large.");
                pngWidth = Integer.MIN_VALUE;
            }
        } catch (NumberFormatException nfe) {
            resp.sendError(HttpServletResponse.SC_PRECONDITION_FAILED,
                    "Missing legal width value.");
            pngWidth = Integer.MIN_VALUE;
        }
        return pngWidth;
    }
}
