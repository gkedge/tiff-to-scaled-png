package com.kedges.tiffscaledpng;

import java.io.*;
import java.net.URISyntaxException;
import java.util.concurrent.*;

/**
 * Test converting file-based TIFFs to file-based PNGs.
 * <p/>
 * gkedge - 12/26/13
 */
public class TiffFileToScaledPngFile {

    private final ExecutorService executor = Executors.newFixedThreadPool(TiffToScaledPng.N_THREADS);

    private void convertImages() throws InterruptedException, IOException, URISyntaxException {
        long start = System.currentTimeMillis();
        File imageDirectory = new File("12149651");
        File[] images = imageDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".tif");
            }
        });
        int imageCount1 = 0;

        for (int ii = 0; ii < 500; ii++) {
            for (File image : images) {
                executor.submit(new TiffFileToPngImageFile(imageCount1++, image));
            }
        }
        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
        // Wait until all threads are finish
        executor.awaitTermination(300, TimeUnit.SECONDS);
        int imageCount = imageCount1;

        float totalTime = System.currentTimeMillis() - start;
        System.out.println("\nTime for " + imageCount + " images: " +
                totalTime + " Avg: " + (totalTime / imageCount));
    }

    private class TiffFileToPngImageFile implements Runnable {
        private final int imageNumber;
        private final File image;

        TiffFileToPngImageFile(int imageNumber, File image) {
            this.imageNumber = imageNumber;
            this.image = image;
        }

        @Override
        public void run() {
            try {
                long start = System.currentTimeMillis();
                InputStream tiffImageFile = new BufferedInputStream(new FileInputStream(image));
                OutputStream pngImageFile =
                        new BufferedOutputStream(new FileOutputStream("test" + imageNumber % 100 + ".png"));

                State imageState = getScaledPngFromTiff(tiffImageFile, 816);

                byte[] pngImage = imageState.getPngImage();

                pngImageFile.write(pngImage);
                pngImageFile.flush();
                pngImageFile.close();
                imageState.updateReport("host", image.getCanonicalPath(), start);
            } catch (IOException e) {
                System.out.println("Failed to save image conversion failure to error log.\n");
                e.printStackTrace();
                System.exit(0);
            } finally {
                if (imageNumber % 100 == 0) {
                    System.out.print(imageNumber % 1000 == 0 ? "\n" : ".");
                }
            }
        }
    }

    private static State getScaledPngFromTiff(InputStream tiffImage,
                                              int pngWidth) throws IOException {
        State imageState = null;
        try {
            int available = tiffImage.available();
            byte[] tiffImageBuf = new byte[available];
            int read = tiffImage.read(tiffImageBuf, 0, available);
            if (read != available) {
                return null;
            }

            imageState = new State(State.ImageChain.LEPTONICA, "resource", tiffImageBuf, pngWidth);
        } finally {
            tiffImage.close();
        }

        TiffToScaledPngJNI.convertTiffToScaledPng(imageState);
        return imageState;
    }

    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
        new TiffFileToScaledPngFile().convertImages();
    }
}
