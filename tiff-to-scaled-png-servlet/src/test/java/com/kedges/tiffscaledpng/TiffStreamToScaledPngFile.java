package com.kedges.tiffscaledpng;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Test streaming TIFFs from a PE2E Web Server and the converting them to file-based PNGs.
 * <p/>
 * gkedge - 12/26/13
 */
public class TiffStreamToScaledPngFile {

    private TiffToScaledPng tiffToScaledPng = new TiffToScaledPng();

    private final ExecutorService executor = Executors.newFixedThreadPool(TiffToScaledPng.N_THREADS);

    {
        try {
            tiffToScaledPng.init(null);
        } catch (ServletException e) {
            e.printStackTrace();
            System.out.println("Failed to init HTTP client.\n" + new Exception());
            System.exit(0);
        }
    }

    public void convertImages() throws InterruptedException, IOException, URISyntaxException {
        List<String> paths = new LinkedList<String>();

        for (int page = 1; page <= 11; page++) {
            paths.add(getUriForPage(page));
        }

        long start = System.currentTimeMillis();
        int imageCount1 = 0;

        for (int ii = 0; ii < 100; ii++) {
            for (String imagePath : paths) {
                executor.submit(new TiffStreamToPngImageFile(imageCount1++, imagePath));
            }
        }
        // This will make the executor accept no new threads
        // and finish all existing threads in the queue
        executor.shutdown();
        // Wait until all threads are finish
        executor.awaitTermination(300, TimeUnit.SECONDS);
        int imageCount = imageCount1;

        float totalTime = System.currentTimeMillis() - start;
        System.out.println("\nTime for " + imageCount + " images: " +
                totalTime + " Avg: " + (totalTime / imageCount));
    }

    private class TiffStreamToPngImageFile implements Runnable {
        private final int imageNumber;
        private final String path;

        public TiffStreamToPngImageFile(int imageNumber, String path) {
            this.imageNumber = imageNumber;
            this.path = path;
        }

        @Override
        public void run() {

            try {
                OutputStream pngImageFile =
                        new BufferedOutputStream(new FileOutputStream("test" + imageNumber % 100 + ".png"));
                int code = tiffToScaledPng.convertTiffFromPe2eWebServer(path, 816, pngImageFile);

                if (code > 300) {
                    String msg =
                            code == HttpServletResponse.SC_NOT_FOUND ? "TIFF does not exist on PE2E Web Server." :
                                    code == HttpServletResponse.SC_BAD_REQUEST ? "Requested PNG is too large." : "I dunno...";
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (imageNumber % 100 == 0) {
                    System.out.print(imageNumber % 1000 == 0 ? "\n" : ".");
                }
            }
        }
    }

    private static String getUriForPage(int page) throws URISyntaxException {
        return "/pe2e-proxy/documents/786/images/" + page;
    }

    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
        new TiffStreamToScaledPngFile().convertImages();
    }
}
