package com.kedges.tiffscaledpng.app;

import com.kedges.tiffscaledpng.State;
import com.kedges.tiffscaledpng.TiffToScaledPngJNI;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.net.URL;

/**
 * greg - 11/16/14
 */
@Path("/scaledpng/{imageName}/")
public class TiffToScaledPngResource {

    /** Read all tiff images into memory. */
    private static final String RANDOM_ACCESS_MODE = "r";
    private static final String IO_EXCEPTION_ERROR_MESSAGE = "File size >= 2 GB";

    @GET
    @Produces("image/png")
    public Response getScaledPng(@PathParam("imageName") String imageName,
                                 @QueryParam("imageChain") String imageChain,
                                 @QueryParam("width") Integer width) throws Exception {
        return buildImageStream(imageName, imageChain, width);
    }

    /**
     * @param imageName
     *         Media file
     * @return Response with Streaming output
     * @throws Exception
     *         IOException if an error occurs in streaming.
     */
    private Response buildImageStream(final String imageName, String imageChain, Integer width) throws Exception {
        String tiffImageFileName = "/" + imageName + ".tif";

        URL url = this.getClass().getResource(tiffImageFileName);
        RandomAccessFile tiffTestFile = new RandomAccessFile(url.getFile(), RANDOM_ACCESS_MODE);
        // Get and check length
        long longlength = tiffTestFile.length();
        int length = (int) longlength;
        if (length != longlength) {
            tiffTestFile.close();
            throw new IOException(IO_EXCEPTION_ERROR_MESSAGE);
        }
        // Read TIFF into buffer set in State.
        byte[] data = new byte[length];
        tiffTestFile.readFully(data);
        tiffTestFile.close();

        final State imageState =
                new State(State.ImageChain.valueOf(imageChain), tiffImageFileName,
                        data, width);

        TiffToScaledPngJNI.convertTiffToScaledPng(imageState);
        final byte[] pngImage = imageState.getPngImage();
        StreamingOutput streamer = new StreamingOutput() {
            @Override
            public void write(final OutputStream pngImageStream) throws IOException, WebApplicationException {
                pngImageStream.write(pngImage);
                pngImageStream.flush();
                pngImageStream.close();
            }
        };

        return Response.ok(streamer).status(200)
                .header("Content-Disposition",  "attachment; filename=\"" + imageName + ".png\"")
                .header(HttpHeaders.CONTENT_LENGTH, pngImage.length).build();
    }

}
