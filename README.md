This project is built using Gradle 2.1.  
`% brew install groovy`  
`% brew install gradle`  

The native JNI code depends on two graphic libraries:  
* Leptonica  
* OpenCV  

`% brew install leptonica --with-libtiff`  
`% brew tap homebrew/science`  
`% brew install opencv`  

In ~/.bash_login:  
`export DYLD_LIBRARY_PATH='/usr/local/lib'`
